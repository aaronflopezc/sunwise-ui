import React from 'react';

import BookmarkIcon from '@mui/icons-material/Bookmark';
import BookmarkBorderIcon from '@mui/icons-material/BookmarkBorder';
import Favorite from '@mui/icons-material/Favorite';
import FavoriteBorder from '@mui/icons-material/FavoriteBorder';

import { Checkbox, FormControl, FormGroup, FormControlLabel } from '../.';

export default {
    title: 'Components/Inputs/Checkbox',
    component: Checkbox,
    argTypes: {
        checked: {
            control: 'boolean',
        },
        color: {
            control: 'select',
            description: '',
            options: [
                'error',
                'info',
                'primary',
                'secondary',
                'success',
                'warning',
            ],
            table: {
                defaultValue: {
                    summary: 'primary',
                },
            },
        },
        disabled: {
            control: 'boolean',
        },
        size: {
            control: 'select',
            options: ['small', 'medium', 'large'],
            table: {
                defaultValue: {
                    summary: 'medium',
                },
            },
        },
    },
};

const Basic = ({ ...args }) => (
    <>
        <Checkbox {...args} defaultChecked />
        <Checkbox {...args} />
        <Checkbox {...args} disabled />
        <Checkbox {...args} disabled checked />
    </>
);

const LabelTemplate = () => (
    <FormGroup>
        <FormControlLabel control={<Checkbox defaultChecked />} label="Label" />
        <FormControlLabel disabled control={<Checkbox />} label="Disabled" />
    </FormGroup>
);

const _Icons = () => (
    <>
        <Checkbox icon={<FavoriteBorder />} checkedIcon={<Favorite />} />
        <Checkbox
            icon={<BookmarkBorderIcon />}
            checkedIcon={<BookmarkIcon />}
        />
    </>
);

const LabelPlacementTemplate = () => (
    <FormControl component="fieldset">
        <FormGroup row>
            <FormControlLabel
                value="top"
                control={<Checkbox />}
                label="Top"
                labelPlacement="top"
            />
            <FormControlLabel
                value="start"
                control={<Checkbox />}
                label="Start"
                labelPlacement="start"
            />
            <FormControlLabel
                value="bottom"
                control={<Checkbox />}
                label="Bottom"
                labelPlacement="bottom"
            />
            <FormControlLabel
                value="end"
                control={<Checkbox />}
                label="End"
                labelPlacement="end"
            />
        </FormGroup>
    </FormControl>
);

export const basic = Basic.bind({});
export const Labels = LabelTemplate.bind({});
export const LabelPlacement = LabelPlacementTemplate.bind({});
export const Icons = _Icons.bind({});
