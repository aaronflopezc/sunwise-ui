import React from 'react';

import { Box, Slider } from '../.';

export default {
    title: 'Components/Inputs/Slider',
    component: Slider,
    argTypes: {
        color: {
            control: 'select',
            description: '',
            options: [
                'error',
                'info',
                'primary',
                'secondary',
                'success',
                'warning',
            ],
            table: {
                defaultValue: {
                    summary: 'primary',
                },
            },
        },
        disabled: {
            control: 'boolean',
            defaultValue: false,
        },
        size: {
            control: 'select',
            options: ['small', 'medium'],
            table: {
                defaultValue: {
                    summary: 'medium',
                },
            },
        },
    },
};

const Basic = ({ ...args }) => {
    const [value, setValue] = React.useState(30);

    const handleChange = (_, newValue) => setValue(newValue);

    const marks = [
        {
            value: 0,
            label: '0',
        },
        {
            value: 50,
            label: '50',
        },
        {
            value: 100,
            label: '100',
        },
    ];

    return (
        <Box sx={{ pt: 3 }}>
            <Slider
                disabled={args.disabled}
                color={args.color}
                size={args.size}
                value={value}
                marks={marks}
                onChange={handleChange}
                valueLabelDisplay="auto"
            />
        </Box>
    );
};

export const basic = Basic.bind({});
