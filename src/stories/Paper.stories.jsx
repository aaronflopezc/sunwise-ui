import React from 'react';

import { styled } from '@mui/material/styles';

import { Box, Grid, Paper } from '../.';

export default {
    title: 'Components/Surfaces/Paper',
    component: Paper,
};

const Basic = () => (
    <Box
        sx={{
            display: 'flex',
            flexWrap: 'wrap',
            '& > :not(style)': {
                m: 1,
                width: 128,
                height: 128,
            },
        }}
    >
        <Paper elevation={0} />
        <Paper />
        <Paper elevation={3} />
    </Box>
);

const Elevation = () => {
    const Item = styled(Paper)(({ theme }) => ({
        ...theme.typography.body2,
        textAlign: 'center',
        color: theme.palette.text.secondary,
        height: 60,
        lineHeight: '60px',
    }));

    return (
        <Grid container spacing={2}>
            <Grid item xs={6}>
                <Box
                    sx={{
                        p: 2,
                        bgcolor: 'background.default',
                        display: 'grid',
                        gridTemplateColumns: { md: '1fr 1fr' },
                        gap: 2,
                    }}
                >
                    {[0, 1, 2, 3, 4, 6, 8, 12, 16, 24].map((elevation) => (
                        <Item key={elevation} elevation={elevation}>
                            {`elevation=${elevation}`}
                        </Item>
                    ))}
                </Box>
            </Grid>
        </Grid>
    );
};

export const basic = Basic.bind({});
export const elevation = Elevation.bind({});
