import React from 'react';

import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import GrainIcon from '@mui/icons-material/Grain';
import HomeIcon from '@mui/icons-material/Home';
import NavigateNextIcon from '@mui/icons-material/NavigateNext';
import WhatshotIcon from '@mui/icons-material/Whatshot';
import { emphasize, styled } from '@mui/material/styles';

import { Breadcrumbs, Chip, Link, Stack, Typography } from '../.';

export default {
    title: 'Components/Navigation/Breadcrumbs',
    component: Breadcrumbs,
};

function handleClick(event) {
    event.preventDefault();
    console.info('You clicked a breadcrumb.');
}

const Basic = () => (
    <Breadcrumbs aria-label="breadcrumb">
        <Link underline="hover" color="inherit" href="/">
            MUI
        </Link>
        <Link
            underline="hover"
            color="inherit"
            href="/material-ui/getting-started/installation/"
        >
            Core
        </Link>
        <Typography color="text.primary">Breadcrumbs</Typography>
    </Breadcrumbs>
);

const ActiveLastBreadcrumb = () => {
    return (
        <div role="presentation" onClick={handleClick}>
            <Breadcrumbs aria-label="breadcrumb">
                <Link underline="hover" color="inherit" href="/">
                    MUI
                </Link>
                <Link
                    underline="hover"
                    color="inherit"
                    href="/material-ui/getting-started/installation/"
                >
                    Core
                </Link>
                <Link
                    underline="hover"
                    color="text.primary"
                    href="/material-ui/react-breadcrumbs/"
                    aria-current="page"
                >
                    Breadcrumbs
                </Link>
            </Breadcrumbs>
        </div>
    );
};

const CustomSeparator = () => {
    const breadcrumbs = [
        <Link
            underline="hover"
            key="1"
            color="inherit"
            href="/"
            onClick={handleClick}
        >
            MUI
        </Link>,
        <Link
            underline="hover"
            key="2"
            color="inherit"
            href="/material-ui/getting-started/installation/"
            onClick={handleClick}
        >
            Core
        </Link>,
        <Typography key="3" color="text.primary">
            Breadcrumb
        </Typography>,
    ];

    return (
        <Stack spacing={2}>
            <Breadcrumbs separator="›" aria-label="breadcrumb">
                {breadcrumbs}
            </Breadcrumbs>
            <Breadcrumbs separator="-" aria-label="breadcrumb">
                {breadcrumbs}
            </Breadcrumbs>
            <Breadcrumbs
                separator={<NavigateNextIcon fontSize="small" />}
                aria-label="breadcrumb"
            >
                {breadcrumbs}
            </Breadcrumbs>
        </Stack>
    );
};

const IconBreadcrumbs = () => {
    return (
        <div role="presentation" onClick={handleClick}>
            <Breadcrumbs aria-label="breadcrumb">
                <Link
                    underline="hover"
                    sx={{ display: 'flex', alignItems: 'center' }}
                    color="inherit"
                    href="/"
                >
                    <HomeIcon sx={{ mr: 0.5 }} fontSize="inherit" />
                    MUI
                </Link>
                <Link
                    underline="hover"
                    sx={{ display: 'flex', alignItems: 'center' }}
                    color="inherit"
                    href="/material-ui/getting-started/installation/"
                >
                    <WhatshotIcon sx={{ mr: 0.5 }} fontSize="inherit" />
                    Core
                </Link>
                <Typography
                    sx={{ display: 'flex', alignItems: 'center' }}
                    color="text.primary"
                >
                    <GrainIcon sx={{ mr: 0.5 }} fontSize="inherit" />
                    Breadcrumb
                </Typography>
            </Breadcrumbs>
        </div>
    );
};

const CollapsedBreadcrumbs = () => {
    return (
        <div role="presentation" onClick={handleClick}>
            <Breadcrumbs maxItems={2} aria-label="breadcrumb">
                <Link underline="hover" color="inherit" href="#">
                    Home
                </Link>
                <Link underline="hover" color="inherit" href="#">
                    Catalog
                </Link>
                <Link underline="hover" color="inherit" href="#">
                    Accessories
                </Link>
                <Link underline="hover" color="inherit" href="#">
                    New Collection
                </Link>
                <Typography color="text.primary">Belts</Typography>
            </Breadcrumbs>
        </div>
    );
};

const CustomizedBreadcrumbs = () => {
    const StyledBreadcrumb = styled(Chip)(({ theme }) => {
        const backgroundColor =
            theme.palette.mode === 'light'
                ? theme.palette.grey[100]
                : theme.palette.grey[800];
        return {
            backgroundColor,
            height: theme.spacing(3),
            color: theme.palette.text.primary,
            fontWeight: theme.typography.fontWeightRegular,
            '&:hover, &:focus': {
                backgroundColor: emphasize(backgroundColor, 0.06),
            },
            '&:active': {
                boxShadow: theme.shadows[1],
                backgroundColor: emphasize(backgroundColor, 0.12),
            },
        };
    });

    return (
        <div role="presentation" onClick={handleClick}>
            <Breadcrumbs aria-label="breadcrumb">
                <StyledBreadcrumb
                    component="a"
                    href="#"
                    label="Home"
                    icon={<HomeIcon fontSize="small" />}
                />
                <StyledBreadcrumb component="a" href="#" label="Catalog" />
                <StyledBreadcrumb
                    label="Accessories"
                    deleteIcon={<ExpandMoreIcon />}
                    onDelete={handleClick}
                />
            </Breadcrumbs>
        </div>
    );
};

export const basic = Basic.bind({});
export const activeLastBreadcrumb = ActiveLastBreadcrumb.bind({});
export const customSeparator = CustomSeparator.bind({});
export const iconBreadcrumbs = IconBreadcrumbs.bind({});
export const collapsedBreadcrumbs = CollapsedBreadcrumbs.bind({});
export const customizedBreadcrumbs = CustomizedBreadcrumbs.bind({});
