import React from 'react';

import DoneIcon from '@mui/icons-material/Done';
import TagFacesIcon from '@mui/icons-material/TagFaces';
import PropTypes from 'prop-types';

import { Avatar, Chip } from '../.';

export default {
    title: 'Components/Data Display/Chip',
    component: Chip,
    argTypes: {
        avatar: {
            control: 'select',
            options: ['none', 'avatar'],
            table: { defaultValue: { summary: 'none' } },
        },
        color: {
            control: 'select',
            options: [
                'error',
                'info',
                'primary',
                'secondary',
                'success',
                'warning',
            ],
        },
        label: {
            control: 'text',
            defaultValue: 'Content',
        },
        icon: {
            control: 'select',
            options: ['none', 'icon'],
            table: { defaultValue: { summary: 'none' } },
        },
        size: {
            control: 'select',
            options: ['small', 'medium'],
            table: { defaultValue: { summary: 'medium' } },
        },
        onDelete: {
            control: 'select',
            defaultValue: 'none',
            options: ['none', 'default', 'custom'],
            table: { defaultValue: { summary: 'none' } },
        },
        variant: {
            control: 'select',
            options: ['filled', 'outlined'],
            table: { defaultValue: { summary: 'filled' } },
        },
    },
};

const Basic = ({ avatar, icon, onDelete, ...args }) => {
    const props = {
        ...args,
    };

    if (avatar === 'avatar') props.avatar = <Avatar />;
    if (icon === 'icon') props.icon = <TagFacesIcon />;
    if (onDelete && onDelete !== 'none') {
        props.onDelete = () => {};

        if (onDelete === 'custom') props.deleteIcon = <DoneIcon />;
    }

    return <Chip {...props} />;
};
export const basic = Basic.bind({});
Basic.propTypes = {
    avatar: PropTypes.string,
    icon: PropTypes.string,
    onDelete: PropTypes.string,
};
