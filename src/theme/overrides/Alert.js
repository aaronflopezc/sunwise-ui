const Alert = (theme) => ({
    MuiAlert: {
        styleOverrides: {
            root: { marginBottom: theme.spacing(2) },
        },
    },
});

export default Alert;
