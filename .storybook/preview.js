import { createTheme } from '@mui/material/styles';
import { muiTheme } from 'storybook-addon-material-ui5';

import breakpoints from '../src/theme/breakpoints';
import componentsOverride from '../src/theme/overrides';
import palette from '../src/theme/palette';
import shadows, { customShadows } from '../src/theme/shadows';
import shape from '../src/theme/shape';
import typography from '../src/theme/typography';

const themeOptions = {
    breakpoints,
    customShadows,
    palette,
    shadows,
    shape,
    typography,
};

const theme = createTheme({
    themeName: 'Light Theme (Sunwise)',
    ...themeOptions,
});

theme.components = componentsOverride(theme);

export const parameters = {
    actions: { argTypesRegex: '^on[A-Z].*' },
    controls: {
        matchers: {
            color: /(background|color)$/i,
            date: /Date$/,
        },
    },
};

export const decorators = [muiTheme([theme])];
