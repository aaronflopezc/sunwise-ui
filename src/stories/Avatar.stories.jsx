import React from 'react';

import { styled } from '@mui/material/styles';

import { Avatar, Badge } from '../.';

export default {
    title: 'Components/Data Display/Avatar',
    component: Avatar,
    argTypes: {
        content: { control: 'text', defaultValue: 'SW' },
        color: { control: 'color' },
        variant: {
            control: 'select',
            options: ['default', 'square', 'rounded'],
        },
        size: { control: 'number', defaultValue: null },
    },
};

const Basic = ({ ...args }) => (
    <Avatar
        {...args}
        sx={{ bgcolor: args.color, width: args.size, height: args.size }}
    >
        {args.content}
    </Avatar>
);

const WithBadge = () => {
    const StyledBadge = styled(Badge)(({ theme }) => ({
        '& .MuiBadge-badge': {
            backgroundColor: '#44b700',
            color: '#44b700',
            boxShadow: `0 0 0 2px ${theme.palette.background.paper}`,
            '&::after': {
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                height: '100%',
                borderRadius: '50%',
                animation: 'ripple 1.2s infinite ease-in-out',
                border: '1px solid currentColor',
                content: '""',
            },
        },
        '@keyframes ripple': {
            '0%': {
                transform: 'scale(.8)',
                opacity: 1,
            },
            '100%': {
                transform: 'scale(2.4)',
                opacity: 0,
            },
        },
    }));

    return (
        <StyledBadge
            overlap="circular"
            anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
            variant="dot"
        >
            <Avatar />
        </StyledBadge>
    );
};

export const basic = Basic.bind({});
export const withBadge = WithBadge.bind({});
