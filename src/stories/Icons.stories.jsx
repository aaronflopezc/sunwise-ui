import React from 'react';

import * as mui from '@mui/icons-material';
import SearchIcon from '@mui/icons-material/Search';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import FormControlLabel from '@mui/material/FormControlLabel';
import Grid from '@mui/material/Grid';
import IconButton from '@mui/material/IconButton';
import InputBase from '@mui/material/InputBase';
import MuiPaper from '@mui/material/Paper';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import { styled } from '@mui/material/styles';
import SvgIcon from '@mui/material/SvgIcon';
import Tooltip from '@mui/material/Tooltip';
import Typography from '@mui/material/Typography';
import { debounce } from '@mui/material/utils';
import { Index as FlexSearchIndex } from 'flexsearch';
import PropTypes from 'prop-types';

import synonyms from './synonyms';

export default {
    title: 'Components/Data Display/Icons',
};

const UPDATE_SEARCH_INDEX_WAIT_MS = 220;

function selectNode(node) {
    // Clear any current selection
    const selection = window.getSelection();
    selection.removeAllRanges();

    // Select code
    const range = document.createRange();
    range.selectNodeContents(node);
    selection.addRange(range);
}

const StyledIcon = styled('span')(({ theme }) => ({
    display: 'inline-flex',
    flexDirection: 'column',
    color: theme.palette.text.secondary,
    margin: '0 4px',
    '& > div': {
        display: 'flex',
    },
    '& > div > *': {
        flexGrow: 1,
        fontSize: '.6rem',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        textAlign: 'center',
        width: 0,
    },
}));

const StyledSvgIcon = styled(SvgIcon)(({ theme }) => ({
    boxSizing: 'content-box',
    cursor: 'pointer',
    color: theme.palette.text.primary,
    borderRadius: theme.shape.borderRadius,
    transition: theme.transitions.create(['background-color', 'box-shadow'], {
        duration: theme.transitions.duration.shortest,
    }),
    padding: theme.spacing(2),
    margin: theme.spacing(0.5, 0),
    '&:hover': {
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[1],
    },
}));

const Icons = React.memo(function Icons(props) {
    const { icons, handleOpenClick } = props;

    const handleLabelClick = (event) => {
        selectNode(event.currentTarget);
    };

    return (
        <div>
            {icons.map((icon) => {
                return (
                    <StyledIcon key={icon.importName}>
                        <StyledSvgIcon
                            component={icon.Component}
                            fontSize="large"
                            tabIndex={-1}
                            onClick={handleOpenClick}
                            title={icon.importName}
                        />
                        <div>
                            <div onClick={handleLabelClick}>
                                {icon.importName}
                            </div>
                        </div>
                    </StyledIcon>
                );
            })}
        </div>
    );
});

Icons.propTypes = {
    handleOpenClick: PropTypes.func.isRequired,
    icons: PropTypes.array.isRequired,
};

const Markdown = styled(MuiPaper)(() => ({
    '&': { padding: '16px', backgroundColor: '#96c6fd80' },
}));

const Title = styled(Typography)(({ theme }) => ({
    display: 'inline-block',
    cursor: 'pointer',
    transition: theme.transitions.create('background-color', {
        duration: theme.transitions.duration.shortest,
    }),
    '&:hover': {
        backgroundColor: '#96c6fd80',
    },
}));

const CanvasComponent = styled(Box)(({ theme }) => ({
    fontSize: 210,
    marginTop: theme.spacing(2),
    color: theme.palette.text.primary,
    backgroundSize: '30px 30px',
    backgroundColor: 'transparent',
    backgroundPosition: '0 0, 0 15px, 15px -15px, -15px 0',
    backgroundImage:
        theme.palette.mode === 'light'
            ? 'linear-gradient(45deg, #e6e6e6 25%, transparent 25%), linear-gradient(-45deg, #e6e6e6 25%, transparent 25%), linear-gradient(45deg, transparent 75%, #e6e6e6 75%), linear-gradient(-45deg, transparent 75%, #e6e6e6 75%)'
            : 'linear-gradient(45deg, #595959 25%, transparent 25%), linear-gradient(-45deg, #595959 25%, transparent 25%), linear-gradient(45deg, transparent 75%, #595959 75%), linear-gradient(-45deg, transparent 75%, #595959 75%)',
}));

const FontSizeComponent = styled('span')(({ theme }) => ({
    margin: theme.spacing(2),
}));

const ContextComponent = styled(Box, {
    shouldForwardProp: (prop) => prop !== 'contextColor',
})(({ theme, contextColor }) => ({
    margin: theme.spacing(0.5),
    padding: theme.spacing(1, 2),
    borderRadius: theme.shape.borderRadius,
    boxSizing: 'content-box',
    ...(contextColor === 'primary' && {
        color: theme.palette.primary.main,
    }),
    ...(contextColor === 'primaryInverse' && {
        color: theme.palette.primary.contrastText,
        backgroundColor: theme.palette.primary.main,
    }),
    ...(contextColor === 'textPrimary' && {
        color: theme.palette.text.primary,
    }),
    ...(contextColor === 'textPrimaryInverse' && {
        color: theme.palette.background.paper,
        backgroundColor: theme.palette.text.primary,
    }),
    ...(contextColor === 'textSecondary' && {
        color: theme.palette.text.secondary,
    }),
    ...(contextColor === 'textSecondaryInverse' && {
        color: theme.palette.background.paper,
        backgroundColor: theme.palette.text.secondary,
    }),
}));

const DialogDetails = React.memo(function DialogDetails(props) {
    const { open, selectedIcon, handleClose } = props;

    return (
        <Dialog fullWidth maxWidth="sm" open={open} onClose={handleClose}>
            {selectedIcon ? (
                <React.Fragment>
                    <DialogTitle>
                        <Title component="span" variant="inherit">
                            {selectedIcon.importName}
                        </Title>
                    </DialogTitle>

                    <Markdown>
                        {`import ${selectedIcon.importName}Icon from
                            '@mui/icons-material/${selectedIcon.importName}';`}
                    </Markdown>

                    <DialogContent>
                        <Grid container spacing={0}>
                            <Grid item xs>
                                <Grid
                                    container
                                    justifyContent="center"
                                    spacing={0}
                                >
                                    <CanvasComponent
                                        component={selectedIcon.Component}
                                    />
                                </Grid>
                            </Grid>
                            <Grid item xs>
                                <Grid
                                    container
                                    alignItems="flex-end"
                                    justifyContent="center"
                                    spacing={0}
                                >
                                    <Grid item>
                                        <Tooltip title="fontSize small">
                                            <FontSizeComponent
                                                as={selectedIcon.Component}
                                                fontSize="small"
                                            />
                                        </Tooltip>
                                    </Grid>
                                    <Grid item>
                                        <Tooltip title="fontSize medium">
                                            <FontSizeComponent
                                                as={selectedIcon.Component}
                                            />
                                        </Tooltip>
                                    </Grid>
                                    <Grid item>
                                        <Tooltip title="fontSize large">
                                            <FontSizeComponent
                                                as={selectedIcon.Component}
                                                fontSize="large"
                                            />
                                        </Tooltip>
                                    </Grid>
                                </Grid>
                                <Grid
                                    container
                                    justifyContent="center"
                                    spacing={0}
                                >
                                    <ContextComponent
                                        component={selectedIcon.Component}
                                        contextColor="primary"
                                    />
                                    <ContextComponent
                                        component={selectedIcon.Component}
                                        contextColor="primaryInverse"
                                    />
                                </Grid>
                                <Grid
                                    container
                                    justifyContent="center"
                                    spacing={0}
                                >
                                    <ContextComponent
                                        component={selectedIcon.Component}
                                        contextColor="textPrimary"
                                    />
                                    <ContextComponent
                                        component={selectedIcon.Component}
                                        contextColor="textPrimaryInverse"
                                    />
                                </Grid>
                                <Grid
                                    container
                                    justifyContent="center"
                                    spacing={0}
                                >
                                    <ContextComponent
                                        component={selectedIcon.Component}
                                        contextColor="textSecondary"
                                    />
                                    <ContextComponent
                                        component={selectedIcon.Component}
                                        contextColor="textSecondaryInverse"
                                    />
                                </Grid>
                            </Grid>
                        </Grid>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleClose}>Close</Button>
                    </DialogActions>
                </React.Fragment>
            ) : (
                <div />
            )}
        </Dialog>
    );
});

DialogDetails.propTypes = {
    handleClose: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired,
    selectedIcon: PropTypes.object,
};

const Form = styled('form')(({ theme }) => ({
    margin: theme.spacing(2, 0),
}));

const _Paper = styled(MuiPaper)(({ theme }) => ({
    position: 'sticky',
    top: 1,
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    marginBottom: theme.spacing(2),
    width: '100%',
}));

const Input = styled(InputBase)({
    marginLeft: 8,
    flex: 1,
});

const searchIndex = new FlexSearchIndex({
    tokenize: 'full',
});

const allIconsMap = {};
const allIcons = Object.keys(mui)
    .sort()
    .map((importName) => {
        let theme;
        if (importName.indexOf('Outlined') !== -1) {
            theme = 'Outlined';
        } else if (importName.indexOf('TwoTone') !== -1) {
            theme = 'Two tone';
        } else if (importName.indexOf('Rounded') !== -1) {
            theme = 'Rounded';
        } else if (importName.indexOf('Sharp') !== -1) {
            theme = 'Sharp';
        } else {
            theme = 'Filled';
        }

        const name = importName.replace(
            /(Outlined|TwoTone|Rounded|Sharp)$/,
            ''
        );
        let searchable = name;
        if (synonyms[searchable]) {
            searchable += ` ${synonyms[searchable]}`;
        }
        searchIndex.addAsync(importName, searchable);

        const icon = {
            importName,
            name,
            theme,
            Component: mui[importName],
        };
        allIconsMap[importName] = icon;
        return icon;
    });

/**
 * Returns the last defined value that has been passed in [value]
 */
function useLatest(value) {
    const latest = React.useRef(value);
    React.useEffect(() => {
        if (value !== undefined && value !== null) {
            latest.current = value;
        }
    }, [value]);
    return value ?? latest.current;
}

function SearchIcons() {
    const [keys, setKeys] = React.useState(null);
    const [theme, setTheme] = React.useState('Filled');
    const [selectedIcon, setSelectedIcon] = React.useState('');
    const [query, setQuery] = React.useState('');

    const handleOpenClick = React.useCallback(
        (event) => {
            setSelectedIcon(event.currentTarget.getAttribute('title'));
        },
        [setSelectedIcon]
    );

    const handleClose = React.useCallback(() => {
        setSelectedIcon('');
    }, [setSelectedIcon]);

    const updateSearchResults = React.useMemo(
        () =>
            debounce((value) => {
                if (value === '') {
                    setKeys(null);
                } else {
                    searchIndex
                        .searchAsync(value, { limit: 3000 })
                        .then((results) => setKeys(results));
                }
            }, UPDATE_SEARCH_INDEX_WAIT_MS),
        []
    );

    React.useEffect(() => {
        updateSearchResults(query);
        return () => {
            updateSearchResults.clear();
        };
    }, [query, updateSearchResults]);

    const icons = React.useMemo(
        () =>
            (keys === null
                ? allIcons
                : keys.map((key) => allIconsMap[key])
            ).filter((icon) => theme === icon.theme),
        [theme, keys]
    );

    const dialogSelectedIcon = useLatest(
        selectedIcon ? allIconsMap[selectedIcon] : null
    );

    return (
        <Grid container spacing={0} sx={{ minHeight: 500 }}>
            <Grid item xs={18} sm={3}>
                <Form>
                    <RadioGroup>
                        {[
                            'Filled',
                            'Outlined',
                            'Rounded',
                            'Two tone',
                            'Sharp',
                        ].map((currentTheme) => {
                            return (
                                <FormControlLabel
                                    key={currentTheme}
                                    control={
                                        <Radio
                                            checked={theme === currentTheme}
                                            onChange={() =>
                                                setTheme(currentTheme)
                                            }
                                            value={currentTheme}
                                        />
                                    }
                                    label={currentTheme}
                                />
                            );
                        })}
                    </RadioGroup>
                </Form>
            </Grid>
            <Grid item xs={18} sm={15}>
                <_Paper>
                    <IconButton sx={{ padding: '10px' }} aria-label="search">
                        <SearchIcon />
                    </IconButton>
                    <Input
                        autoFocus
                        value={query}
                        onChange={(event) => setQuery(event.target.value)}
                        placeholder="Search icons…"
                        inputProps={{ 'aria-label': 'search icons' }}
                    />
                </_Paper>
                <Typography sx={{ mb: 1 }}>
                    {icons.length} matching results
                </Typography>
                <Icons icons={icons} handleOpenClick={handleOpenClick} />
            </Grid>
            <DialogDetails
                open={!!selectedIcon}
                selectedIcon={dialogSelectedIcon}
                handleClose={handleClose}
            />
        </Grid>
    );
}

export const overview = SearchIcons.bind({});
