const TextField = () => ({
    MuiTextField: {
        defaultProps: {
            size: 'small',
        },
    },
});

export default TextField;
