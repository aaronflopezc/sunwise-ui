import React from 'react';

import FavoriteIcon from '@mui/icons-material/Favorite';
import PersonPinIcon from '@mui/icons-material/PersonPin';
import PhoneIcon from '@mui/icons-material/Phone';
import PhoneMissedIcon from '@mui/icons-material/PhoneMissed';
import PropTypes from 'prop-types';

import { Box, Tabs, Tab } from '../.';

export default {
    title: 'Components/Navigation/Tabs',
    component: Tabs,
    argTypes: {
        textColor: {
            control: 'select',
            options: ['primary', 'secondary'],
            table: { defaultValue: { summary: 'primary' } },
        },
        indicatorColor: {
            control: 'select',
            options: ['primary', 'secondary'],
            table: { defaultValue: { summary: 'primary' } },
        },
        centered: {
            control: 'boolean',
        },
        disabled: {
            control: 'boolean',
        },
        size: {
            control: 'select',
            options: ['small', 'medium', 'large'],
            table: { defaultValue: { summary: 'medium' } },
        },
        variant: {
            control: 'select',
            options: ['default', 'fullWidth', 'scrollable'],
            table: { defaultValue: { summary: 'default' } },
        },
    },
};

const TabPanel = (props) => {
    const { children, selectedTab, value, ...other } = props;

    return (
        <div
            aria-labelledby={`tab-${value}`}
            hidden={value !== selectedTab}
            id={`tabpanel-${value}`}
            role="tabpanel"
            {...other}
        >
            <Box sx={{ p: 2 }}>{value === selectedTab && <>{children}</>}</Box>
        </div>
    );
};

TabPanel.propTypes = {
    children: PropTypes.node,
    value: PropTypes.number.isRequired,
    selectedTab: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

const Basic = ({ ...args }) => {
    const [selectedTab, setSelectedTab] = React.useState(0);
    const handleChange = (_, newValue) => setSelectedTab(newValue);

    return (
        <Box sx={{ width: '100%' }}>
            <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                <Tabs
                    {...args}
                    onChange={handleChange}
                    scrollButtons="auto"
                    allowScrollButtonsMobile
                    value={selectedTab}
                >
                    <Tab label="Item One" value={0} />
                    <Tab label="Item Two" value={1} disabled />
                    <Tab label="Item Three" value={2} />
                    <Tab label="Item Four" value={3} />
                    <Tab label="Item Five" value={4} />
                    <Tab label="Item Six" value={5} />
                    <Tab label="Item Seven" value={6} />
                </Tabs>
            </Box>
            <TabPanel selectedTab={selectedTab} value={0}>
                Item One
            </TabPanel>
            <TabPanel selectedTab={selectedTab} value={1}>
                Item Two
            </TabPanel>
            <TabPanel selectedTab={selectedTab} value={2}>
                Item Three
            </TabPanel>
            <TabPanel selectedTab={selectedTab} value={3}>
                Item Four
            </TabPanel>
            <TabPanel selectedTab={selectedTab} value={4}>
                Item Five
            </TabPanel>
            <TabPanel selectedTab={selectedTab} value={5}>
                Item Six
            </TabPanel>
            <TabPanel selectedTab={selectedTab} value={6}>
                Item Seven
            </TabPanel>
        </Box>
    );
};

const VerticalTabs = () => {
    const [selectedTab, setSelectedTab] = React.useState(0);
    const handleChange = (_, newValue) => setSelectedTab(newValue);

    return (
        <Box
            sx={{
                flexGrow: 1,
                bgcolor: 'background.paper',
                display: 'flex',
                height: 224,
            }}
        >
            <Tabs
                orientation="vertical"
                variant="scrollable"
                value={selectedTab}
                onChange={handleChange}
                sx={{ borderRight: 1, borderColor: 'divider' }}
            >
                <Tab label="Item One" value={0} />
                <Tab label="Item Two" value={1} />
                <Tab label="Item Three" value={2} />
                <Tab label="Item Four" value={3} />
                <Tab label="Item Five" value={4} />
                <Tab label="Item Six" value={5} />
                <Tab label="Item Seven" value={6} />
            </Tabs>
            <TabPanel selectedTab={selectedTab} value={0}>
                Item One
            </TabPanel>
            <TabPanel selectedTab={selectedTab} value={1}>
                Item Two
            </TabPanel>
            <TabPanel selectedTab={selectedTab} value={2}>
                Item Three
            </TabPanel>
            <TabPanel selectedTab={selectedTab} value={3}>
                Item Four
            </TabPanel>
            <TabPanel selectedTab={selectedTab} value={4}>
                Item Five
            </TabPanel>
            <TabPanel selectedTab={selectedTab} value={5}>
                Item Six
            </TabPanel>
            <TabPanel selectedTab={selectedTab} value={6}>
                Item Seven
            </TabPanel>
        </Box>
    );
};

const IconTabs = () => {
    const [selectedTab, setSelectedTab] = React.useState(0);
    const handleChange = (_, newValue) => setSelectedTab(newValue);

    return (
        <Tabs value={selectedTab} onChange={handleChange}>
            <Tab icon={<PhoneIcon />} aria-label="phone" />
            <Tab icon={<FavoriteIcon />} aria-label="favorite" />
            <Tab icon={<PersonPinIcon />} aria-label="person" />
        </Tabs>
    );
};

const IconPosition = () => {
    const [selectedTab, setSelectedTab] = React.useState(0);
    const handleChange = (_, newValue) => setSelectedTab(newValue);

    return (
        <Tabs value={selectedTab} onChange={handleChange}>
            <Tab icon={<PhoneIcon />} label="top" />
            <Tab
                icon={<PhoneMissedIcon />}
                iconPosition="start"
                label="start"
            />
            <Tab icon={<FavoriteIcon />} iconPosition="end" label="end" />
            <Tab
                icon={<PersonPinIcon />}
                iconPosition="bottom"
                label="bottom"
            />
        </Tabs>
    );
};

export const basic = Basic.bind({});
export const verticalTabs = VerticalTabs.bind({});
export const iconTabs = IconTabs.bind({});
export const iconPosition = IconPosition.bind({});
