const Tooltip = (theme) => ({
    MuiTooltip: {
        defaultProps: { arrow: true },
        styleOverrides: {
            arrow: { color: '#f5f5f9' },
            tooltip: {
                backgroundColor: '#f5f5f9',
                color: 'rgba(0, 0, 0, 0.87)',
                fontSize: theme.typography.pxToRem(12),
            },
        },
    },
});

export default Tooltip;
