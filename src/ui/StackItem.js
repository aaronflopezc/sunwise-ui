import Paper from '@mui/material/Paper';
import { styled } from '@mui/material/styles';

const StackItem = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    padding: theme.spacing(2),
}));

export default StackItem;
