import { useGridApiContext, useGridSelector, gridPageSelector, gridPageCountSelector } from '@mui/x-data-grid';
export { DataGrid } from '@mui/x-data-grid';
export { default as Accordion } from '@mui/material/Accordion';
export { default as AccordionDetails } from '@mui/material/AccordionDetails';
export { default as AccordionSummary } from '@mui/material/AccordionSummary';
export { default as Alert } from '@mui/material/Alert';
export { default as AlertTitle } from '@mui/material/AlertTitle';
export { default as AppBar } from '@mui/material/AppBar';
export { default as Autocomplete } from '@mui/material/Autocomplete';
export { default as Avatar } from '@mui/material/Avatar';
export { default as Backdrop } from '@mui/material/Backdrop';
export { default as Badge } from '@mui/material/Badge';
import React, { forwardRef } from 'react';
import Box from '@mui/material/Box';
export { default as Box } from '@mui/material/Box';
import Divider from '@mui/material/Divider';
export { default as Divider } from '@mui/material/Divider';
export { default as Breadcrumbs } from '@mui/material/Breadcrumbs';
import Button$1 from '@mui/material/Button';
export { default as ButtonGroup } from '@mui/material/ButtonGroup';
import Card$1 from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
export { default as CardActions } from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
export { default as CardContent } from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
export { default as CardHeader } from '@mui/material/CardHeader';
export { default as Checkbox } from '@mui/material/Checkbox';
export { default as Chip } from '@mui/material/Chip';
export { default as ClickAwayListener } from '@mui/material/ClickAwayListener';
export { default as Collapse } from '@mui/material/Collapse';
import Dialog$1 from '@mui/material/Dialog';
export { default as Dialog } from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
export { default as DialogActions } from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
export { default as DialogContent } from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
export { default as DialogTitle } from '@mui/material/DialogTitle';
import { useTranslation, initReactI18next } from 'react-i18next';
export { default as Container } from '@mui/material/Container';
import Pagination from '@mui/material/Pagination';
export { default as Pagination } from '@mui/material/Pagination';
export { default as DialogContentText } from '@mui/material/DialogContentText';
export { default as Drawer } from '@mui/material/Drawer';
export { default as FormControl } from '@mui/material/FormControl';
export { default as FormControlLabel } from '@mui/material/FormControlLabel';
export { default as FormGroup } from '@mui/material/FormGroup';
export { default as FormHelperText } from '@mui/material/FormHelperText';
export { default as FormLabel } from '@mui/material/FormLabel';
export { default as Grid } from '@mui/material/Grid';
import IconButton$1 from '@mui/material/IconButton';
export { default as IconButton } from '@mui/material/IconButton';
export { default as Input } from '@mui/material/Input';
export { default as InputAdornment } from '@mui/material/InputAdornment';
export { default as InputBase } from '@mui/material/InputBase';
export { default as InputLabel } from '@mui/material/InputLabel';
export { default as Link } from '@mui/material/Link';
export { default as List } from '@mui/material/List';
export { default as ListItem } from '@mui/material/ListItem';
export { default as ListItemAvatar } from '@mui/material/ListItemAvatar';
export { default as ListItemButton } from '@mui/material/ListItemButton';
export { default as ListItemIcon } from '@mui/material/ListItemIcon';
export { default as ListItemText } from '@mui/material/ListItemText';
export { default as ListSubheader } from '@mui/material/ListSubheader';
export { default as Menu } from '@mui/material/Menu';
export { default as MenuItem } from '@mui/material/MenuItem';
export { default as MenuList } from '@mui/material/MenuList';
export { default as Modal } from '@mui/material/Modal';
export { default as OutlinedInput } from '@mui/material/OutlinedInput';
export { default as PaginationItem } from '@mui/material/PaginationItem';
import Paper$1 from '@mui/material/Paper';
export { default as Paper } from '@mui/material/Paper';
export { default as Popover } from '@mui/material/Popover';
export { default as Radio } from '@mui/material/Radio';
export { default as RadioGroup } from '@mui/material/RadioGroup';
export { default as Select } from '@mui/material/Select';
export { default as Skeleton } from '@mui/material/Skeleton';
export { default as Slider } from '@mui/material/Slider';
export { default as Snackbar } from '@mui/material/Snackbar';
export { default as Stack } from '@mui/material/Stack';
import { styled, alpha, createTheme, StyledEngineProvider, ThemeProvider } from '@mui/material/styles';
export { default as SvgIcon } from '@mui/material/SvgIcon';
export { default as SwipeableDrawer } from '@mui/material/SwipeableDrawer';
export { default as Switch } from '@mui/material/Switch';
export { default as Tab } from '@mui/material/Tab';
export { default as Table } from '@mui/material/Table';
export { default as TableBody } from '@mui/material/TableBody';
export { default as TableCell } from '@mui/material/TableCell';
export { default as TableContainer } from '@mui/material/TableContainer';
export { default as TableFooter } from '@mui/material/TableFooter';
export { default as TableHead } from '@mui/material/TableHead';
export { default as TablePagination } from '@mui/material/TablePagination';
export { default as TableRow } from '@mui/material/TableRow';
export { default as Tabs } from '@mui/material/Tabs';
export { default as TextField } from '@mui/material/TextField';
import { CssBaseline } from '@mui/material';
import { merge } from 'lodash';
export { default as ToggleButton } from '@mui/material/ToggleButton';
export { default as ToggleButtonGroup } from '@mui/material/ToggleButtonGroup';
export { default as Toolbar } from '@mui/material/Toolbar';
export { default as Tooltip } from '@mui/material/Tooltip';
export { default as Typography } from '@mui/material/Typography';
import CloseIcon from '@mui/icons-material/Close';
import i18n from 'i18next';
import LanguageDetector from 'i18next-browser-languagedetector';

function createCommonjsModule(fn, module) {
	return module = { exports: {} }, fn(module, module.exports), module.exports;
}

/** @license React v16.13.1
 * react-is.production.min.js
 *
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
var b="function"===typeof Symbol&&Symbol.for,c=b?Symbol.for("react.element"):60103,d=b?Symbol.for("react.portal"):60106,e=b?Symbol.for("react.fragment"):60107,f=b?Symbol.for("react.strict_mode"):60108,g=b?Symbol.for("react.profiler"):60114,h=b?Symbol.for("react.provider"):60109,k=b?Symbol.for("react.context"):60110,l=b?Symbol.for("react.async_mode"):60111,m=b?Symbol.for("react.concurrent_mode"):60111,n=b?Symbol.for("react.forward_ref"):60112,p=b?Symbol.for("react.suspense"):60113,q=b?
Symbol.for("react.suspense_list"):60120,r=b?Symbol.for("react.memo"):60115,t=b?Symbol.for("react.lazy"):60116,v=b?Symbol.for("react.block"):60121,w=b?Symbol.for("react.fundamental"):60117,x=b?Symbol.for("react.responder"):60118,y=b?Symbol.for("react.scope"):60119;
function z(a){if("object"===typeof a&&null!==a){var u=a.$$typeof;switch(u){case c:switch(a=a.type,a){case l:case m:case e:case g:case f:case p:return a;default:switch(a=a&&a.$$typeof,a){case k:case n:case t:case r:case h:return a;default:return u}}case d:return u}}}function A(a){return z(a)===m}var AsyncMode=l;var ConcurrentMode=m;var ContextConsumer=k;var ContextProvider=h;var Element=c;var ForwardRef=n;var Fragment$1=e;var Lazy=t;var Memo=r;var Portal=d;
var Profiler=g;var StrictMode=f;var Suspense=p;var isAsyncMode=function(a){return A(a)||z(a)===l};var isConcurrentMode=A;var isContextConsumer=function(a){return z(a)===k};var isContextProvider=function(a){return z(a)===h};var isElement=function(a){return "object"===typeof a&&null!==a&&a.$$typeof===c};var isForwardRef=function(a){return z(a)===n};var isFragment=function(a){return z(a)===e};var isLazy=function(a){return z(a)===t};
var isMemo=function(a){return z(a)===r};var isPortal=function(a){return z(a)===d};var isProfiler=function(a){return z(a)===g};var isStrictMode=function(a){return z(a)===f};var isSuspense=function(a){return z(a)===p};
var isValidElementType=function(a){return "string"===typeof a||"function"===typeof a||a===e||a===m||a===g||a===f||a===p||a===q||"object"===typeof a&&null!==a&&(a.$$typeof===t||a.$$typeof===r||a.$$typeof===h||a.$$typeof===k||a.$$typeof===n||a.$$typeof===w||a.$$typeof===x||a.$$typeof===y||a.$$typeof===v)};var typeOf=z;

var reactIs_production_min = {
	AsyncMode: AsyncMode,
	ConcurrentMode: ConcurrentMode,
	ContextConsumer: ContextConsumer,
	ContextProvider: ContextProvider,
	Element: Element,
	ForwardRef: ForwardRef,
	Fragment: Fragment$1,
	Lazy: Lazy,
	Memo: Memo,
	Portal: Portal,
	Profiler: Profiler,
	StrictMode: StrictMode,
	Suspense: Suspense,
	isAsyncMode: isAsyncMode,
	isConcurrentMode: isConcurrentMode,
	isContextConsumer: isContextConsumer,
	isContextProvider: isContextProvider,
	isElement: isElement,
	isForwardRef: isForwardRef,
	isFragment: isFragment,
	isLazy: isLazy,
	isMemo: isMemo,
	isPortal: isPortal,
	isProfiler: isProfiler,
	isStrictMode: isStrictMode,
	isSuspense: isSuspense,
	isValidElementType: isValidElementType,
	typeOf: typeOf
};

var reactIs_development = createCommonjsModule(function (module, exports) {



if (process.env.NODE_ENV !== "production") {
  (function() {

// The Symbol used to tag the ReactElement-like types. If there is no native Symbol
// nor polyfill, then a plain number is used for performance.
var hasSymbol = typeof Symbol === 'function' && Symbol.for;
var REACT_ELEMENT_TYPE = hasSymbol ? Symbol.for('react.element') : 0xeac7;
var REACT_PORTAL_TYPE = hasSymbol ? Symbol.for('react.portal') : 0xeaca;
var REACT_FRAGMENT_TYPE = hasSymbol ? Symbol.for('react.fragment') : 0xeacb;
var REACT_STRICT_MODE_TYPE = hasSymbol ? Symbol.for('react.strict_mode') : 0xeacc;
var REACT_PROFILER_TYPE = hasSymbol ? Symbol.for('react.profiler') : 0xead2;
var REACT_PROVIDER_TYPE = hasSymbol ? Symbol.for('react.provider') : 0xeacd;
var REACT_CONTEXT_TYPE = hasSymbol ? Symbol.for('react.context') : 0xeace; // TODO: We don't use AsyncMode or ConcurrentMode anymore. They were temporary
// (unstable) APIs that have been removed. Can we remove the symbols?

var REACT_ASYNC_MODE_TYPE = hasSymbol ? Symbol.for('react.async_mode') : 0xeacf;
var REACT_CONCURRENT_MODE_TYPE = hasSymbol ? Symbol.for('react.concurrent_mode') : 0xeacf;
var REACT_FORWARD_REF_TYPE = hasSymbol ? Symbol.for('react.forward_ref') : 0xead0;
var REACT_SUSPENSE_TYPE = hasSymbol ? Symbol.for('react.suspense') : 0xead1;
var REACT_SUSPENSE_LIST_TYPE = hasSymbol ? Symbol.for('react.suspense_list') : 0xead8;
var REACT_MEMO_TYPE = hasSymbol ? Symbol.for('react.memo') : 0xead3;
var REACT_LAZY_TYPE = hasSymbol ? Symbol.for('react.lazy') : 0xead4;
var REACT_BLOCK_TYPE = hasSymbol ? Symbol.for('react.block') : 0xead9;
var REACT_FUNDAMENTAL_TYPE = hasSymbol ? Symbol.for('react.fundamental') : 0xead5;
var REACT_RESPONDER_TYPE = hasSymbol ? Symbol.for('react.responder') : 0xead6;
var REACT_SCOPE_TYPE = hasSymbol ? Symbol.for('react.scope') : 0xead7;

function isValidElementType(type) {
  return typeof type === 'string' || typeof type === 'function' || // Note: its typeof might be other than 'symbol' or 'number' if it's a polyfill.
  type === REACT_FRAGMENT_TYPE || type === REACT_CONCURRENT_MODE_TYPE || type === REACT_PROFILER_TYPE || type === REACT_STRICT_MODE_TYPE || type === REACT_SUSPENSE_TYPE || type === REACT_SUSPENSE_LIST_TYPE || typeof type === 'object' && type !== null && (type.$$typeof === REACT_LAZY_TYPE || type.$$typeof === REACT_MEMO_TYPE || type.$$typeof === REACT_PROVIDER_TYPE || type.$$typeof === REACT_CONTEXT_TYPE || type.$$typeof === REACT_FORWARD_REF_TYPE || type.$$typeof === REACT_FUNDAMENTAL_TYPE || type.$$typeof === REACT_RESPONDER_TYPE || type.$$typeof === REACT_SCOPE_TYPE || type.$$typeof === REACT_BLOCK_TYPE);
}

function typeOf(object) {
  if (typeof object === 'object' && object !== null) {
    var $$typeof = object.$$typeof;

    switch ($$typeof) {
      case REACT_ELEMENT_TYPE:
        var type = object.type;

        switch (type) {
          case REACT_ASYNC_MODE_TYPE:
          case REACT_CONCURRENT_MODE_TYPE:
          case REACT_FRAGMENT_TYPE:
          case REACT_PROFILER_TYPE:
          case REACT_STRICT_MODE_TYPE:
          case REACT_SUSPENSE_TYPE:
            return type;

          default:
            var $$typeofType = type && type.$$typeof;

            switch ($$typeofType) {
              case REACT_CONTEXT_TYPE:
              case REACT_FORWARD_REF_TYPE:
              case REACT_LAZY_TYPE:
              case REACT_MEMO_TYPE:
              case REACT_PROVIDER_TYPE:
                return $$typeofType;

              default:
                return $$typeof;
            }

        }

      case REACT_PORTAL_TYPE:
        return $$typeof;
    }
  }

  return undefined;
} // AsyncMode is deprecated along with isAsyncMode

var AsyncMode = REACT_ASYNC_MODE_TYPE;
var ConcurrentMode = REACT_CONCURRENT_MODE_TYPE;
var ContextConsumer = REACT_CONTEXT_TYPE;
var ContextProvider = REACT_PROVIDER_TYPE;
var Element = REACT_ELEMENT_TYPE;
var ForwardRef = REACT_FORWARD_REF_TYPE;
var Fragment = REACT_FRAGMENT_TYPE;
var Lazy = REACT_LAZY_TYPE;
var Memo = REACT_MEMO_TYPE;
var Portal = REACT_PORTAL_TYPE;
var Profiler = REACT_PROFILER_TYPE;
var StrictMode = REACT_STRICT_MODE_TYPE;
var Suspense = REACT_SUSPENSE_TYPE;
var hasWarnedAboutDeprecatedIsAsyncMode = false; // AsyncMode should be deprecated

function isAsyncMode(object) {
  {
    if (!hasWarnedAboutDeprecatedIsAsyncMode) {
      hasWarnedAboutDeprecatedIsAsyncMode = true; // Using console['warn'] to evade Babel and ESLint

      console['warn']('The ReactIs.isAsyncMode() alias has been deprecated, ' + 'and will be removed in React 17+. Update your code to use ' + 'ReactIs.isConcurrentMode() instead. It has the exact same API.');
    }
  }

  return isConcurrentMode(object) || typeOf(object) === REACT_ASYNC_MODE_TYPE;
}
function isConcurrentMode(object) {
  return typeOf(object) === REACT_CONCURRENT_MODE_TYPE;
}
function isContextConsumer(object) {
  return typeOf(object) === REACT_CONTEXT_TYPE;
}
function isContextProvider(object) {
  return typeOf(object) === REACT_PROVIDER_TYPE;
}
function isElement(object) {
  return typeof object === 'object' && object !== null && object.$$typeof === REACT_ELEMENT_TYPE;
}
function isForwardRef(object) {
  return typeOf(object) === REACT_FORWARD_REF_TYPE;
}
function isFragment(object) {
  return typeOf(object) === REACT_FRAGMENT_TYPE;
}
function isLazy(object) {
  return typeOf(object) === REACT_LAZY_TYPE;
}
function isMemo(object) {
  return typeOf(object) === REACT_MEMO_TYPE;
}
function isPortal(object) {
  return typeOf(object) === REACT_PORTAL_TYPE;
}
function isProfiler(object) {
  return typeOf(object) === REACT_PROFILER_TYPE;
}
function isStrictMode(object) {
  return typeOf(object) === REACT_STRICT_MODE_TYPE;
}
function isSuspense(object) {
  return typeOf(object) === REACT_SUSPENSE_TYPE;
}

exports.AsyncMode = AsyncMode;
exports.ConcurrentMode = ConcurrentMode;
exports.ContextConsumer = ContextConsumer;
exports.ContextProvider = ContextProvider;
exports.Element = Element;
exports.ForwardRef = ForwardRef;
exports.Fragment = Fragment;
exports.Lazy = Lazy;
exports.Memo = Memo;
exports.Portal = Portal;
exports.Profiler = Profiler;
exports.StrictMode = StrictMode;
exports.Suspense = Suspense;
exports.isAsyncMode = isAsyncMode;
exports.isConcurrentMode = isConcurrentMode;
exports.isContextConsumer = isContextConsumer;
exports.isContextProvider = isContextProvider;
exports.isElement = isElement;
exports.isForwardRef = isForwardRef;
exports.isFragment = isFragment;
exports.isLazy = isLazy;
exports.isMemo = isMemo;
exports.isPortal = isPortal;
exports.isProfiler = isProfiler;
exports.isStrictMode = isStrictMode;
exports.isSuspense = isSuspense;
exports.isValidElementType = isValidElementType;
exports.typeOf = typeOf;
  })();
}
});

var reactIs = createCommonjsModule(function (module) {

if (process.env.NODE_ENV === 'production') {
  module.exports = reactIs_production_min;
} else {
  module.exports = reactIs_development;
}
});

/*
object-assign
(c) Sindre Sorhus
@license MIT
*/
/* eslint-disable no-unused-vars */
var getOwnPropertySymbols = Object.getOwnPropertySymbols;
var hasOwnProperty = Object.prototype.hasOwnProperty;
var propIsEnumerable = Object.prototype.propertyIsEnumerable;

function toObject(val) {
	if (val === null || val === undefined) {
		throw new TypeError('Object.assign cannot be called with null or undefined');
	}

	return Object(val);
}

function shouldUseNative() {
	try {
		if (!Object.assign) {
			return false;
		}

		// Detect buggy property enumeration order in older V8 versions.

		// https://bugs.chromium.org/p/v8/issues/detail?id=4118
		var test1 = new String('abc');  // eslint-disable-line no-new-wrappers
		test1[5] = 'de';
		if (Object.getOwnPropertyNames(test1)[0] === '5') {
			return false;
		}

		// https://bugs.chromium.org/p/v8/issues/detail?id=3056
		var test2 = {};
		for (var i = 0; i < 10; i++) {
			test2['_' + String.fromCharCode(i)] = i;
		}
		var order2 = Object.getOwnPropertyNames(test2).map(function (n) {
			return test2[n];
		});
		if (order2.join('') !== '0123456789') {
			return false;
		}

		// https://bugs.chromium.org/p/v8/issues/detail?id=3056
		var test3 = {};
		'abcdefghijklmnopqrst'.split('').forEach(function (letter) {
			test3[letter] = letter;
		});
		if (Object.keys(Object.assign({}, test3)).join('') !==
				'abcdefghijklmnopqrst') {
			return false;
		}

		return true;
	} catch (err) {
		// We don't expect any of the above to throw, but better to be safe.
		return false;
	}
}

var objectAssign = shouldUseNative() ? Object.assign : function (target, source) {
	var from;
	var to = toObject(target);
	var symbols;

	for (var s = 1; s < arguments.length; s++) {
		from = Object(arguments[s]);

		for (var key in from) {
			if (hasOwnProperty.call(from, key)) {
				to[key] = from[key];
			}
		}

		if (getOwnPropertySymbols) {
			symbols = getOwnPropertySymbols(from);
			for (var i = 0; i < symbols.length; i++) {
				if (propIsEnumerable.call(from, symbols[i])) {
					to[symbols[i]] = from[symbols[i]];
				}
			}
		}
	}

	return to;
};

/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

var ReactPropTypesSecret = 'SECRET_DO_NOT_PASS_THIS_OR_YOU_WILL_BE_FIRED';

var ReactPropTypesSecret_1 = ReactPropTypesSecret;

var has = Function.call.bind(Object.prototype.hasOwnProperty);

var printWarning = function() {};

if (process.env.NODE_ENV !== 'production') {
  var ReactPropTypesSecret$1 = ReactPropTypesSecret_1;
  var loggedTypeFailures = {};
  var has$1 = has;

  printWarning = function(text) {
    var message = 'Warning: ' + text;
    if (typeof console !== 'undefined') {
      console.error(message);
    }
    try {
      // --- Welcome to debugging React ---
      // This error was thrown as a convenience so that you can use this stack
      // to find the callsite that caused this warning to fire.
      throw new Error(message);
    } catch (x) { /**/ }
  };
}

/**
 * Assert that the values match with the type specs.
 * Error messages are memorized and will only be shown once.
 *
 * @param {object} typeSpecs Map of name to a ReactPropType
 * @param {object} values Runtime values that need to be type-checked
 * @param {string} location e.g. "prop", "context", "child context"
 * @param {string} componentName Name of the component for error messages.
 * @param {?Function} getStack Returns the component stack.
 * @private
 */
function checkPropTypes(typeSpecs, values, location, componentName, getStack) {
  if (process.env.NODE_ENV !== 'production') {
    for (var typeSpecName in typeSpecs) {
      if (has$1(typeSpecs, typeSpecName)) {
        var error;
        // Prop type validation may throw. In case they do, we don't want to
        // fail the render phase where it didn't fail before. So we log it.
        // After these have been cleaned up, we'll let them throw.
        try {
          // This is intentionally an invariant that gets caught. It's the same
          // behavior as without this statement except with a better message.
          if (typeof typeSpecs[typeSpecName] !== 'function') {
            var err = Error(
              (componentName || 'React class') + ': ' + location + ' type `' + typeSpecName + '` is invalid; ' +
              'it must be a function, usually from the `prop-types` package, but received `' + typeof typeSpecs[typeSpecName] + '`.' +
              'This often happens because of typos such as `PropTypes.function` instead of `PropTypes.func`.'
            );
            err.name = 'Invariant Violation';
            throw err;
          }
          error = typeSpecs[typeSpecName](values, typeSpecName, componentName, location, null, ReactPropTypesSecret$1);
        } catch (ex) {
          error = ex;
        }
        if (error && !(error instanceof Error)) {
          printWarning(
            (componentName || 'React class') + ': type specification of ' +
            location + ' `' + typeSpecName + '` is invalid; the type checker ' +
            'function must return `null` or an `Error` but returned a ' + typeof error + '. ' +
            'You may have forgotten to pass an argument to the type checker ' +
            'creator (arrayOf, instanceOf, objectOf, oneOf, oneOfType, and ' +
            'shape all require an argument).'
          );
        }
        if (error instanceof Error && !(error.message in loggedTypeFailures)) {
          // Only monitor this failure once because there tends to be a lot of the
          // same error.
          loggedTypeFailures[error.message] = true;

          var stack = getStack ? getStack() : '';

          printWarning(
            'Failed ' + location + ' type: ' + error.message + (stack != null ? stack : '')
          );
        }
      }
    }
  }
}

/**
 * Resets warning cache when testing.
 *
 * @private
 */
checkPropTypes.resetWarningCache = function() {
  if (process.env.NODE_ENV !== 'production') {
    loggedTypeFailures = {};
  }
};

var checkPropTypes_1 = checkPropTypes;

var printWarning$1 = function() {};

if (process.env.NODE_ENV !== 'production') {
  printWarning$1 = function(text) {
    var message = 'Warning: ' + text;
    if (typeof console !== 'undefined') {
      console.error(message);
    }
    try {
      // --- Welcome to debugging React ---
      // This error was thrown as a convenience so that you can use this stack
      // to find the callsite that caused this warning to fire.
      throw new Error(message);
    } catch (x) {}
  };
}

function emptyFunctionThatReturnsNull() {
  return null;
}

var factoryWithTypeCheckers = function(isValidElement, throwOnDirectAccess) {
  /* global Symbol */
  var ITERATOR_SYMBOL = typeof Symbol === 'function' && Symbol.iterator;
  var FAUX_ITERATOR_SYMBOL = '@@iterator'; // Before Symbol spec.

  /**
   * Returns the iterator method function contained on the iterable object.
   *
   * Be sure to invoke the function with the iterable as context:
   *
   *     var iteratorFn = getIteratorFn(myIterable);
   *     if (iteratorFn) {
   *       var iterator = iteratorFn.call(myIterable);
   *       ...
   *     }
   *
   * @param {?object} maybeIterable
   * @return {?function}
   */
  function getIteratorFn(maybeIterable) {
    var iteratorFn = maybeIterable && (ITERATOR_SYMBOL && maybeIterable[ITERATOR_SYMBOL] || maybeIterable[FAUX_ITERATOR_SYMBOL]);
    if (typeof iteratorFn === 'function') {
      return iteratorFn;
    }
  }

  /**
   * Collection of methods that allow declaration and validation of props that are
   * supplied to React components. Example usage:
   *
   *   var Props = require('ReactPropTypes');
   *   var MyArticle = React.createClass({
   *     propTypes: {
   *       // An optional string prop named "description".
   *       description: Props.string,
   *
   *       // A required enum prop named "category".
   *       category: Props.oneOf(['News','Photos']).isRequired,
   *
   *       // A prop named "dialog" that requires an instance of Dialog.
   *       dialog: Props.instanceOf(Dialog).isRequired
   *     },
   *     render: function() { ... }
   *   });
   *
   * A more formal specification of how these methods are used:
   *
   *   type := array|bool|func|object|number|string|oneOf([...])|instanceOf(...)
   *   decl := ReactPropTypes.{type}(.isRequired)?
   *
   * Each and every declaration produces a function with the same signature. This
   * allows the creation of custom validation functions. For example:
   *
   *  var MyLink = React.createClass({
   *    propTypes: {
   *      // An optional string or URI prop named "href".
   *      href: function(props, propName, componentName) {
   *        var propValue = props[propName];
   *        if (propValue != null && typeof propValue !== 'string' &&
   *            !(propValue instanceof URI)) {
   *          return new Error(
   *            'Expected a string or an URI for ' + propName + ' in ' +
   *            componentName
   *          );
   *        }
   *      }
   *    },
   *    render: function() {...}
   *  });
   *
   * @internal
   */

  var ANONYMOUS = '<<anonymous>>';

  // Important!
  // Keep this list in sync with production version in `./factoryWithThrowingShims.js`.
  var ReactPropTypes = {
    array: createPrimitiveTypeChecker('array'),
    bigint: createPrimitiveTypeChecker('bigint'),
    bool: createPrimitiveTypeChecker('boolean'),
    func: createPrimitiveTypeChecker('function'),
    number: createPrimitiveTypeChecker('number'),
    object: createPrimitiveTypeChecker('object'),
    string: createPrimitiveTypeChecker('string'),
    symbol: createPrimitiveTypeChecker('symbol'),

    any: createAnyTypeChecker(),
    arrayOf: createArrayOfTypeChecker,
    element: createElementTypeChecker(),
    elementType: createElementTypeTypeChecker(),
    instanceOf: createInstanceTypeChecker,
    node: createNodeChecker(),
    objectOf: createObjectOfTypeChecker,
    oneOf: createEnumTypeChecker,
    oneOfType: createUnionTypeChecker,
    shape: createShapeTypeChecker,
    exact: createStrictShapeTypeChecker,
  };

  /**
   * inlined Object.is polyfill to avoid requiring consumers ship their own
   * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/is
   */
  /*eslint-disable no-self-compare*/
  function is(x, y) {
    // SameValue algorithm
    if (x === y) {
      // Steps 1-5, 7-10
      // Steps 6.b-6.e: +0 != -0
      return x !== 0 || 1 / x === 1 / y;
    } else {
      // Step 6.a: NaN == NaN
      return x !== x && y !== y;
    }
  }
  /*eslint-enable no-self-compare*/

  /**
   * We use an Error-like object for backward compatibility as people may call
   * PropTypes directly and inspect their output. However, we don't use real
   * Errors anymore. We don't inspect their stack anyway, and creating them
   * is prohibitively expensive if they are created too often, such as what
   * happens in oneOfType() for any type before the one that matched.
   */
  function PropTypeError(message, data) {
    this.message = message;
    this.data = data && typeof data === 'object' ? data: {};
    this.stack = '';
  }
  // Make `instanceof Error` still work for returned errors.
  PropTypeError.prototype = Error.prototype;

  function createChainableTypeChecker(validate) {
    if (process.env.NODE_ENV !== 'production') {
      var manualPropTypeCallCache = {};
      var manualPropTypeWarningCount = 0;
    }
    function checkType(isRequired, props, propName, componentName, location, propFullName, secret) {
      componentName = componentName || ANONYMOUS;
      propFullName = propFullName || propName;

      if (secret !== ReactPropTypesSecret_1) {
        if (throwOnDirectAccess) {
          // New behavior only for users of `prop-types` package
          var err = new Error(
            'Calling PropTypes validators directly is not supported by the `prop-types` package. ' +
            'Use `PropTypes.checkPropTypes()` to call them. ' +
            'Read more at http://fb.me/use-check-prop-types'
          );
          err.name = 'Invariant Violation';
          throw err;
        } else if (process.env.NODE_ENV !== 'production' && typeof console !== 'undefined') {
          // Old behavior for people using React.PropTypes
          var cacheKey = componentName + ':' + propName;
          if (
            !manualPropTypeCallCache[cacheKey] &&
            // Avoid spamming the console because they are often not actionable except for lib authors
            manualPropTypeWarningCount < 3
          ) {
            printWarning$1(
              'You are manually calling a React.PropTypes validation ' +
              'function for the `' + propFullName + '` prop on `' + componentName + '`. This is deprecated ' +
              'and will throw in the standalone `prop-types` package. ' +
              'You may be seeing this warning due to a third-party PropTypes ' +
              'library. See https://fb.me/react-warning-dont-call-proptypes ' + 'for details.'
            );
            manualPropTypeCallCache[cacheKey] = true;
            manualPropTypeWarningCount++;
          }
        }
      }
      if (props[propName] == null) {
        if (isRequired) {
          if (props[propName] === null) {
            return new PropTypeError('The ' + location + ' `' + propFullName + '` is marked as required ' + ('in `' + componentName + '`, but its value is `null`.'));
          }
          return new PropTypeError('The ' + location + ' `' + propFullName + '` is marked as required in ' + ('`' + componentName + '`, but its value is `undefined`.'));
        }
        return null;
      } else {
        return validate(props, propName, componentName, location, propFullName);
      }
    }

    var chainedCheckType = checkType.bind(null, false);
    chainedCheckType.isRequired = checkType.bind(null, true);

    return chainedCheckType;
  }

  function createPrimitiveTypeChecker(expectedType) {
    function validate(props, propName, componentName, location, propFullName, secret) {
      var propValue = props[propName];
      var propType = getPropType(propValue);
      if (propType !== expectedType) {
        // `propValue` being instance of, say, date/regexp, pass the 'object'
        // check, but we can offer a more precise error message here rather than
        // 'of type `object`'.
        var preciseType = getPreciseType(propValue);

        return new PropTypeError(
          'Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + preciseType + '` supplied to `' + componentName + '`, expected ') + ('`' + expectedType + '`.'),
          {expectedType: expectedType}
        );
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createAnyTypeChecker() {
    return createChainableTypeChecker(emptyFunctionThatReturnsNull);
  }

  function createArrayOfTypeChecker(typeChecker) {
    function validate(props, propName, componentName, location, propFullName) {
      if (typeof typeChecker !== 'function') {
        return new PropTypeError('Property `' + propFullName + '` of component `' + componentName + '` has invalid PropType notation inside arrayOf.');
      }
      var propValue = props[propName];
      if (!Array.isArray(propValue)) {
        var propType = getPropType(propValue);
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + propType + '` supplied to `' + componentName + '`, expected an array.'));
      }
      for (var i = 0; i < propValue.length; i++) {
        var error = typeChecker(propValue, i, componentName, location, propFullName + '[' + i + ']', ReactPropTypesSecret_1);
        if (error instanceof Error) {
          return error;
        }
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createElementTypeChecker() {
    function validate(props, propName, componentName, location, propFullName) {
      var propValue = props[propName];
      if (!isValidElement(propValue)) {
        var propType = getPropType(propValue);
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + propType + '` supplied to `' + componentName + '`, expected a single ReactElement.'));
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createElementTypeTypeChecker() {
    function validate(props, propName, componentName, location, propFullName) {
      var propValue = props[propName];
      if (!reactIs.isValidElementType(propValue)) {
        var propType = getPropType(propValue);
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + propType + '` supplied to `' + componentName + '`, expected a single ReactElement type.'));
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createInstanceTypeChecker(expectedClass) {
    function validate(props, propName, componentName, location, propFullName) {
      if (!(props[propName] instanceof expectedClass)) {
        var expectedClassName = expectedClass.name || ANONYMOUS;
        var actualClassName = getClassName(props[propName]);
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + actualClassName + '` supplied to `' + componentName + '`, expected ') + ('instance of `' + expectedClassName + '`.'));
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createEnumTypeChecker(expectedValues) {
    if (!Array.isArray(expectedValues)) {
      if (process.env.NODE_ENV !== 'production') {
        if (arguments.length > 1) {
          printWarning$1(
            'Invalid arguments supplied to oneOf, expected an array, got ' + arguments.length + ' arguments. ' +
            'A common mistake is to write oneOf(x, y, z) instead of oneOf([x, y, z]).'
          );
        } else {
          printWarning$1('Invalid argument supplied to oneOf, expected an array.');
        }
      }
      return emptyFunctionThatReturnsNull;
    }

    function validate(props, propName, componentName, location, propFullName) {
      var propValue = props[propName];
      for (var i = 0; i < expectedValues.length; i++) {
        if (is(propValue, expectedValues[i])) {
          return null;
        }
      }

      var valuesString = JSON.stringify(expectedValues, function replacer(key, value) {
        var type = getPreciseType(value);
        if (type === 'symbol') {
          return String(value);
        }
        return value;
      });
      return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of value `' + String(propValue) + '` ' + ('supplied to `' + componentName + '`, expected one of ' + valuesString + '.'));
    }
    return createChainableTypeChecker(validate);
  }

  function createObjectOfTypeChecker(typeChecker) {
    function validate(props, propName, componentName, location, propFullName) {
      if (typeof typeChecker !== 'function') {
        return new PropTypeError('Property `' + propFullName + '` of component `' + componentName + '` has invalid PropType notation inside objectOf.');
      }
      var propValue = props[propName];
      var propType = getPropType(propValue);
      if (propType !== 'object') {
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + propType + '` supplied to `' + componentName + '`, expected an object.'));
      }
      for (var key in propValue) {
        if (has(propValue, key)) {
          var error = typeChecker(propValue, key, componentName, location, propFullName + '.' + key, ReactPropTypesSecret_1);
          if (error instanceof Error) {
            return error;
          }
        }
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createUnionTypeChecker(arrayOfTypeCheckers) {
    if (!Array.isArray(arrayOfTypeCheckers)) {
      process.env.NODE_ENV !== 'production' ? printWarning$1('Invalid argument supplied to oneOfType, expected an instance of array.') : void 0;
      return emptyFunctionThatReturnsNull;
    }

    for (var i = 0; i < arrayOfTypeCheckers.length; i++) {
      var checker = arrayOfTypeCheckers[i];
      if (typeof checker !== 'function') {
        printWarning$1(
          'Invalid argument supplied to oneOfType. Expected an array of check functions, but ' +
          'received ' + getPostfixForTypeWarning(checker) + ' at index ' + i + '.'
        );
        return emptyFunctionThatReturnsNull;
      }
    }

    function validate(props, propName, componentName, location, propFullName) {
      var expectedTypes = [];
      for (var i = 0; i < arrayOfTypeCheckers.length; i++) {
        var checker = arrayOfTypeCheckers[i];
        var checkerResult = checker(props, propName, componentName, location, propFullName, ReactPropTypesSecret_1);
        if (checkerResult == null) {
          return null;
        }
        if (checkerResult.data && has(checkerResult.data, 'expectedType')) {
          expectedTypes.push(checkerResult.data.expectedType);
        }
      }
      var expectedTypesMessage = (expectedTypes.length > 0) ? ', expected one of type [' + expectedTypes.join(', ') + ']': '';
      return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` supplied to ' + ('`' + componentName + '`' + expectedTypesMessage + '.'));
    }
    return createChainableTypeChecker(validate);
  }

  function createNodeChecker() {
    function validate(props, propName, componentName, location, propFullName) {
      if (!isNode(props[propName])) {
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` supplied to ' + ('`' + componentName + '`, expected a ReactNode.'));
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function invalidValidatorError(componentName, location, propFullName, key, type) {
    return new PropTypeError(
      (componentName || 'React class') + ': ' + location + ' type `' + propFullName + '.' + key + '` is invalid; ' +
      'it must be a function, usually from the `prop-types` package, but received `' + type + '`.'
    );
  }

  function createShapeTypeChecker(shapeTypes) {
    function validate(props, propName, componentName, location, propFullName) {
      var propValue = props[propName];
      var propType = getPropType(propValue);
      if (propType !== 'object') {
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type `' + propType + '` ' + ('supplied to `' + componentName + '`, expected `object`.'));
      }
      for (var key in shapeTypes) {
        var checker = shapeTypes[key];
        if (typeof checker !== 'function') {
          return invalidValidatorError(componentName, location, propFullName, key, getPreciseType(checker));
        }
        var error = checker(propValue, key, componentName, location, propFullName + '.' + key, ReactPropTypesSecret_1);
        if (error) {
          return error;
        }
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createStrictShapeTypeChecker(shapeTypes) {
    function validate(props, propName, componentName, location, propFullName) {
      var propValue = props[propName];
      var propType = getPropType(propValue);
      if (propType !== 'object') {
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type `' + propType + '` ' + ('supplied to `' + componentName + '`, expected `object`.'));
      }
      // We need to check all keys in case some are required but missing from props.
      var allKeys = objectAssign({}, props[propName], shapeTypes);
      for (var key in allKeys) {
        var checker = shapeTypes[key];
        if (has(shapeTypes, key) && typeof checker !== 'function') {
          return invalidValidatorError(componentName, location, propFullName, key, getPreciseType(checker));
        }
        if (!checker) {
          return new PropTypeError(
            'Invalid ' + location + ' `' + propFullName + '` key `' + key + '` supplied to `' + componentName + '`.' +
            '\nBad object: ' + JSON.stringify(props[propName], null, '  ') +
            '\nValid keys: ' + JSON.stringify(Object.keys(shapeTypes), null, '  ')
          );
        }
        var error = checker(propValue, key, componentName, location, propFullName + '.' + key, ReactPropTypesSecret_1);
        if (error) {
          return error;
        }
      }
      return null;
    }

    return createChainableTypeChecker(validate);
  }

  function isNode(propValue) {
    switch (typeof propValue) {
      case 'number':
      case 'string':
      case 'undefined':
        return true;
      case 'boolean':
        return !propValue;
      case 'object':
        if (Array.isArray(propValue)) {
          return propValue.every(isNode);
        }
        if (propValue === null || isValidElement(propValue)) {
          return true;
        }

        var iteratorFn = getIteratorFn(propValue);
        if (iteratorFn) {
          var iterator = iteratorFn.call(propValue);
          var step;
          if (iteratorFn !== propValue.entries) {
            while (!(step = iterator.next()).done) {
              if (!isNode(step.value)) {
                return false;
              }
            }
          } else {
            // Iterator will provide entry [k,v] tuples rather than values.
            while (!(step = iterator.next()).done) {
              var entry = step.value;
              if (entry) {
                if (!isNode(entry[1])) {
                  return false;
                }
              }
            }
          }
        } else {
          return false;
        }

        return true;
      default:
        return false;
    }
  }

  function isSymbol(propType, propValue) {
    // Native Symbol.
    if (propType === 'symbol') {
      return true;
    }

    // falsy value can't be a Symbol
    if (!propValue) {
      return false;
    }

    // 19.4.3.5 Symbol.prototype[@@toStringTag] === 'Symbol'
    if (propValue['@@toStringTag'] === 'Symbol') {
      return true;
    }

    // Fallback for non-spec compliant Symbols which are polyfilled.
    if (typeof Symbol === 'function' && propValue instanceof Symbol) {
      return true;
    }

    return false;
  }

  // Equivalent of `typeof` but with special handling for array and regexp.
  function getPropType(propValue) {
    var propType = typeof propValue;
    if (Array.isArray(propValue)) {
      return 'array';
    }
    if (propValue instanceof RegExp) {
      // Old webkits (at least until Android 4.0) return 'function' rather than
      // 'object' for typeof a RegExp. We'll normalize this here so that /bla/
      // passes PropTypes.object.
      return 'object';
    }
    if (isSymbol(propType, propValue)) {
      return 'symbol';
    }
    return propType;
  }

  // This handles more types than `getPropType`. Only used for error messages.
  // See `createPrimitiveTypeChecker`.
  function getPreciseType(propValue) {
    if (typeof propValue === 'undefined' || propValue === null) {
      return '' + propValue;
    }
    var propType = getPropType(propValue);
    if (propType === 'object') {
      if (propValue instanceof Date) {
        return 'date';
      } else if (propValue instanceof RegExp) {
        return 'regexp';
      }
    }
    return propType;
  }

  // Returns a string that is postfixed to a warning about an invalid type.
  // For example, "undefined" or "of type array"
  function getPostfixForTypeWarning(value) {
    var type = getPreciseType(value);
    switch (type) {
      case 'array':
      case 'object':
        return 'an ' + type;
      case 'boolean':
      case 'date':
      case 'regexp':
        return 'a ' + type;
      default:
        return type;
    }
  }

  // Returns class name of the object, if any.
  function getClassName(propValue) {
    if (!propValue.constructor || !propValue.constructor.name) {
      return ANONYMOUS;
    }
    return propValue.constructor.name;
  }

  ReactPropTypes.checkPropTypes = checkPropTypes_1;
  ReactPropTypes.resetWarningCache = checkPropTypes_1.resetWarningCache;
  ReactPropTypes.PropTypes = ReactPropTypes;

  return ReactPropTypes;
};

function emptyFunction() {}
function emptyFunctionWithReset() {}
emptyFunctionWithReset.resetWarningCache = emptyFunction;

var factoryWithThrowingShims = function() {
  function shim(props, propName, componentName, location, propFullName, secret) {
    if (secret === ReactPropTypesSecret_1) {
      // It is still safe when called from React.
      return;
    }
    var err = new Error(
      'Calling PropTypes validators directly is not supported by the `prop-types` package. ' +
      'Use PropTypes.checkPropTypes() to call them. ' +
      'Read more at http://fb.me/use-check-prop-types'
    );
    err.name = 'Invariant Violation';
    throw err;
  }  shim.isRequired = shim;
  function getShim() {
    return shim;
  }  // Important!
  // Keep this list in sync with production version in `./factoryWithTypeCheckers.js`.
  var ReactPropTypes = {
    array: shim,
    bigint: shim,
    bool: shim,
    func: shim,
    number: shim,
    object: shim,
    string: shim,
    symbol: shim,

    any: shim,
    arrayOf: getShim,
    element: shim,
    elementType: shim,
    instanceOf: getShim,
    node: shim,
    objectOf: getShim,
    oneOf: getShim,
    oneOfType: getShim,
    shape: getShim,
    exact: getShim,

    checkPropTypes: emptyFunctionWithReset,
    resetWarningCache: emptyFunction
  };

  ReactPropTypes.PropTypes = ReactPropTypes;

  return ReactPropTypes;
};

var propTypes = createCommonjsModule(function (module) {
/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

if (process.env.NODE_ENV !== 'production') {
  var ReactIs = reactIs;

  // By explicitly using `prop-types` you are opting into new development behavior.
  // http://fb.me/prop-types-in-prod
  var throwOnDirectAccess = true;
  module.exports = factoryWithTypeCheckers(ReactIs.isElement, throwOnDirectAccess);
} else {
  // By explicitly using `prop-types` you are opting into new production behavior.
  // http://fb.me/prop-types-in-prod
  module.exports = factoryWithThrowingShims();
}
});

var BottomActions = function BottomActions(props) {
  var children = props.children;
  return /*#__PURE__*/React.createElement(Fragment, null, /*#__PURE__*/React.createElement(Divider, {
    sx: {
      mx: '-16px'
    }
  }), /*#__PURE__*/React.createElement(Box, {
    display: "flex",
    gap: "16px",
    justifyContent: "end",
    sx: {
      '& .MuiButton-root': {
        mb: 0
      }
    }
  }, children));
};

BottomActions.propTypes = {
  children: propTypes.oneOfType([propTypes.element, propTypes.node])
};

function _extends() {
  _extends = Object.assign ? Object.assign.bind() : function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };
  return _extends.apply(this, arguments);
}

function _objectWithoutPropertiesLoose(source, excluded) {
  if (source == null) return {};
  var target = {};
  var sourceKeys = Object.keys(source);
  var key, i;

  for (i = 0; i < sourceKeys.length; i++) {
    key = sourceKeys[i];
    if (excluded.indexOf(key) >= 0) continue;
    target[key] = source[key];
  }

  return target;
}

var _excluded = ["children", "dataIntercomTarget", "visible"];
var ButtonComponentForwardRef = forwardRef(function (props, ref) {
  var children = props.children,
      dataIntercomTarget = props.dataIntercomTarget,
      _props$visible = props.visible,
      visible = _props$visible === void 0 ? true : _props$visible,
      rest = _objectWithoutPropertiesLoose(props, _excluded);

  if (!visible) return null;

  var innerProps = _extends({}, rest, {
    'data-intercom-target': dataIntercomTarget
  });

  return /*#__PURE__*/React.createElement(Button$1, _extends({}, innerProps, {
    ref: ref
  }), children);
});
ButtonComponentForwardRef.propTypes = {
  children: propTypes.node,
  dataIntercomTarget: propTypes.string,
  visible: propTypes.bool
};
ButtonComponentForwardRef.displayName = 'Button';

var _excluded$1 = ["children", "sx"];
var _Card = Card$1;
_Card.Body = CardContent;

_Card.Header = function (props) {
  var children = props.children,
      sx = props.sx,
      rest = _objectWithoutPropertiesLoose(props, _excluded$1);

  return /*#__PURE__*/React.createElement(CardContent, _extends({}, rest, {
    sx: _extends({
      borderBottom: 1,
      borderColor: 'divider',
      padding: '16px !important'
    }, sx)
  }), children);
};

_Card.Header.propTypes = {
  children: propTypes.oneOfType([propTypes.object, propTypes.array]),
  sx: propTypes.object
};
_Card.Header.displayName = 'Card.Header';
_Card.Media = CardMedia;
_Card.Actions = CardActions;

var ConfirmDialog = function ConfirmDialog(_ref) {
  var handleClickClose = _ref.handleClickClose,
      handleClickSuccess = _ref.handleClickSuccess,
      _ref$isOpen = _ref.isOpen,
      isOpen = _ref$isOpen === void 0 ? false : _ref$isOpen,
      message = _ref.message,
      title = _ref.title;

  var _useTranslation = useTranslation(),
      t = _useTranslation.t;

  return /*#__PURE__*/React.createElement(Dialog$1, {
    open: isOpen === null ? false : isOpen,
    onClose: handleClickClose
  }, /*#__PURE__*/React.createElement(DialogTitle, null, title), /*#__PURE__*/React.createElement(DialogContent, {
    dividers: true
  }, /*#__PURE__*/React.createElement("h6", null, message)), /*#__PURE__*/React.createElement(DialogActions, null, /*#__PURE__*/React.createElement(ButtonComponentForwardRef, {
    onClick: handleClickClose,
    color: "error"
  }, /*#__PURE__*/React.createElement("strong", null, t('Cancel').toUpperCase())), /*#__PURE__*/React.createElement(ButtonComponentForwardRef, {
    onClick: handleClickSuccess
  }, /*#__PURE__*/React.createElement("strong", null, t('Accept').toUpperCase()))));
};

ConfirmDialog.propTypes = {
  handleClickClose: propTypes.func,
  handleClickSuccess: propTypes.func,
  isOpen: propTypes.bool,
  message: propTypes.oneOfType([propTypes.string, propTypes.object]),
  title: propTypes.string
};

var CustomPagination = function CustomPagination() {
  var apiRef = useGridApiContext();
  var page = useGridSelector(apiRef, gridPageSelector);
  var pageCount = useGridSelector(apiRef, gridPageCountSelector);
  return /*#__PURE__*/React.createElement(Pagination, {
    color: "primary",
    count: pageCount,
    page: page + 1,
    onChange: function onChange(_, value) {
      return apiRef.current.setPage(value - 1);
    }
  });
};

var StackItem = styled(Paper$1)(function (_ref) {
  var theme = _ref.theme;
  return {
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    padding: theme.spacing(2)
  };
});

var _excluded$2 = ["children", "selectedTab", "value"];

var TabPanel = function TabPanel(props) {
  var children = props.children,
      selectedTab = props.selectedTab,
      value = props.value,
      other = _objectWithoutPropertiesLoose(props, _excluded$2);

  return /*#__PURE__*/React.createElement("div", _extends({
    "aria-labelledby": "tab-" + value,
    hidden: value !== selectedTab,
    id: "tabpanel-" + value,
    role: "tabpanel"
  }, other), /*#__PURE__*/React.createElement(Box, {
    sx: {
      py: 2
    }
  }, value === selectedTab && /*#__PURE__*/React.createElement("div", null, children)));
};

TabPanel.propTypes = {
  children: propTypes.oneOfType([propTypes.element, propTypes.node]),
  value: propTypes.oneOfType([propTypes.string, propTypes.number]),
  selectedTab: propTypes.oneOfType([propTypes.string, propTypes.number])
};

var breakpoints = {
  values: {
    xs: 0,
    sm: 600,
    md: 900,
    lg: 1200,
    xl: 1536
  }
};

var Alert = function Alert(theme) {
  return {
    MuiAlert: {
      styleOverrides: {
        root: {
          marginBottom: theme.spacing(2)
        }
      }
    }
  };
};

var Autocomplete = function Autocomplete(theme) {
  return {
    MuiAutocomplete: {
      styleOverrides: {
        paper: {
          boxShadow: theme.customShadows.z20
        }
      }
    }
  };
};

var Backdrop = function Backdrop(theme) {
  var varLow = alpha(theme.palette.grey[900], 0.48);
  var varHigh = alpha(theme.palette.grey[900], 1);
  return {
    MuiBackdrop: {
      styleOverrides: {
        root: {
          background: ["rgb(22,28,36)", "-moz-linear-gradient(75deg, " + varLow + " 0%, " + varHigh + " 100%)", "-webkit-linear-gradient(75deg, " + varLow + " 0%, " + varHigh + " 100%)", "linear-gradient(75deg, " + varLow + " 0%, " + varHigh + " 100%)"],
          '&.MuiBackdrop-invisible': {
            background: 'transparent'
          }
        }
      }
    }
  };
};

var Button = function Button(theme) {
  return {
    MuiToggleButton: {
      styleOverrides: {
        root: {
          '&:hover': {
            boxShadow: 'none'
          },
          borderRadius: 20
        }
      }
    },
    MuiButton: {
      defaultProps: {
        variant: 'contained'
      },
      styleOverrides: {
        root: {
          '&:hover': {
            boxShadow: 'none'
          },
          borderRadius: 20
        },
        sizeLarge: {
          height: 48
        },
        containedInherit: {
          '&:hover': {
            backgroundColor: theme.palette.grey[400]
          },
          boxShadow: theme.customShadows.z8,
          color: theme.palette.grey[800]
        },
        containedPrimary: {
          boxShadow: theme.customShadows.primary
        },
        containedSecondary: {
          boxShadow: theme.customShadows.secondary
        },
        outlinedInherit: {
          '&:hover': {
            backgroundColor: theme.palette.action.hover
          },
          border: "1px solid " + theme.palette.grey['500_32']
        },
        textInherit: {
          '&:hover': {
            backgroundColor: theme.palette.action.hover
          }
        }
      }
    }
  };
};

var Card = function Card(theme) {
  return {
    MuiCard: {
      styleOverrides: {
        root: {
          borderRadius: theme.shape.borderRadiusMd,
          boxShadow: theme.customShadows.z16,
          marginBottom: theme.spacing(2)
        }
      }
    },
    MuiCardHeader: {
      defaultProps: {
        titleTypographyProps: {
          variant: 'h6'
        },
        subheaderTypographyProps: {
          variant: 'body2'
        }
      },
      styleOverrides: {}
    },
    MuiCardContent: {
      styleOverrides: {
        root: {}
      }
    }
  };
};

var Dialog = function Dialog() {
  return {
    MuiDialogContent: {
      styleOverrides: {
        root: {
          overflowY: 'initial'
        }
      }
    }
  };
};

var Grid = function Grid() {
  return {
    MuiGrid: {
      defaultProps: {
        columns: 18,
        spacing: 2
      },
      styleOverrides: {
        container: {
          marginTop: 1
        },
        root: {
          '> .MuiCard-root': {
            marginBottom: 0
          }
        }
      }
    }
  };
};

var IconButton = function IconButton(theme) {
  return {
    MuiIconButton: {
      variants: [{
        props: {
          color: 'default'
        },
        style: {
          '&:hover': {
            backgroundColor: theme.palette.action.hover
          }
        }
      }, {
        props: {
          color: 'inherit'
        },
        style: {
          '&:hover': {
            backgroundColor: theme.palette.action.hover
          }
        }
      }]
    }
  };
};

var Input = function Input(theme) {
  return {
    MuiInputBase: {
      styleOverrides: {
        root: {
          '&.Mui-disabled': {
            '& svg': {
              color: theme.palette.text.disabled
            }
          }
        },
        input: {
          '&::placeholder': {
            opacity: 1,
            color: theme.palette.text.disabled
          }
        }
      }
    },
    MuiInput: {
      styleOverrides: {
        underline: {
          '&:before': {
            borderBottomColor: theme.palette.grey['500_56']
          }
        }
      }
    },
    MuiFilledInput: {
      styleOverrides: {
        root: {
          backgroundColor: theme.palette.grey['500_12'],
          '&:hover': {
            backgroundColor: theme.palette.grey['500_16']
          },
          '&.Mui-focused': {
            backgroundColor: theme.palette.action.focus
          },
          '&.Mui-disabled': {
            backgroundColor: theme.palette.action.disabledBackground
          }
        },
        underline: {
          '&:before': {
            borderBottomColor: theme.palette.grey['500_56']
          }
        }
      }
    },
    MuiOutlinedInput: {}
  };
};

var Paper = function Paper() {
  return {
    MuiPaper: {
      defaultProps: {
        elevation: 0
      },
      styleOverrides: {
        root: {
          backgroundImage: 'none'
        }
      }
    }
  };
};

var Stack = function Stack() {
  return {
    MuiStack: {
      defaultProps: {
        spacing: 2
      }
    }
  };
};

var TextField = function TextField() {
  return {
    MuiTextField: {
      defaultProps: {
        size: 'small'
      }
    }
  };
};

var Tooltip = function Tooltip(theme) {
  return {
    MuiTooltip: {
      defaultProps: {
        arrow: true
      },
      styleOverrides: {
        arrow: {
          color: '#f5f5f9'
        },
        tooltip: {
          backgroundColor: '#f5f5f9',
          color: 'rgba(0, 0, 0, 0.87)',
          fontSize: theme.typography.pxToRem(12)
        }
      }
    }
  };
};

var Typography = function Typography(theme) {
  return {
    MuiTypography: {
      styleOverrides: {
        gutterBottom: {
          marginBottom: theme.spacing(2)
        },
        paragraph: {
          marginBottom: theme.spacing(2)
        }
      }
    }
  };
};

var ComponentsOverrides = function ComponentsOverrides(theme) {
  return merge(Alert(theme), Autocomplete(theme), Backdrop(theme), Button(theme), Card(theme), Dialog(), Grid(), IconButton(theme), Input(theme), Paper(), Stack(), TextField(), Tooltip(theme), Typography(theme));
};

var createGradient = function createGradient(color1, color2) {
  return "linear-gradient(to bottom, " + color1 + ", " + color2 + ")";
};

var GREY = {
  0: '#FFFFFF',
  100: '#F9FAFB',
  200: '#F4F6F8',
  300: '#DFE3E8',
  400: '#C4CDD5',
  500: '#919EAB',
  600: '#637381',
  700: '#454F5B',
  800: '#212B36',
  900: '#161C24',
  '500_8': alpha('#919EAB', 0.08),
  '500_12': alpha('#919EAB', 0.12),
  '500_16': alpha('#919EAB', 0.16),
  '500_24': alpha('#919EAB', 0.24),
  '500_32': alpha('#919EAB', 0.32),
  '500_48': alpha('#919EAB', 0.48),
  '500_56': alpha('#919EAB', 0.56),
  '500_80': alpha('#919EAB', 0.8)
};
var PRIMARY = {
  contrastText: '#fff',
  dark: '#A85B00',
  darker: '#C66B00',
  light: '#FFB843',
  lighter: '#FFCB48',
  main: '#FF9A00'
};
var SECONDARY = {
  contrastText: '#fff',
  dark: '#1939B7',
  darker: '#091A7A',
  light: '#84A9FF',
  lighter: '#D6E4FF',
  main: '#3366FF'
};
var INFO = {
  contrastText: '#fff',
  dark: '#0C53B7',
  darker: '#04297A',
  light: '#74CAFF',
  lighter: '#D0F2FF',
  main: '#1890FF'
};
var SUCCESS = {
  contrastText: GREY[800],
  dark: '#229A16',
  darker: '#08660D',
  light: '#AAF27F',
  lighter: '#E9FCD4',
  main: '#54D62C'
};
var WARNING = {
  contrastText: GREY[800],
  dark: '#B78103',
  darker: '#7A4F01',
  light: '#FFE16A',
  lighter: '#FFF7CD',
  main: '#FFC107'
};
var ERROR = {
  contrastText: '#fff',
  dark: '#B72136',
  darker: '#7A0C2E',
  light: '#FFA48D',
  lighter: '#FFE7D9',
  main: '#FF4842'
};
var GRADIENTS = {
  error: createGradient(ERROR.light, ERROR.main),
  info: createGradient(INFO.light, INFO.main),
  primary: createGradient(PRIMARY.light, PRIMARY.main),
  success: createGradient(SUCCESS.light, SUCCESS.main),
  warning: createGradient(WARNING.light, WARNING.main)
};
var CHART_COLORS = {
  blue: ['#2D99FF', '#83CFFF', '#A5F3FF', '#CCFAFF'],
  green: ['#2CD9C5', '#60F1C8', '#A4F7CC', '#C0F2DC'],
  red: ['#FF6C40', '#FF8F6D', '#FFBD98', '#FFF2D4'],
  violet: ['#826AF9', '#9E86FF', '#D0AEFF', '#F7D2FF'],
  yellow: ['#FFE700', '#FFEF5A', '#FFF7AE', '#FFF3D6']
};
var palette = {
  action: {
    active: GREY[600],
    hover: GREY['500_8'],
    selected: GREY['500_16'],
    disabled: GREY['500_80'],
    disabledBackground: GREY['500_24'],
    focus: GREY['500_24'],
    hoverOpacity: 0.08,
    disabledOpacity: 0.48
  },
  background: {
    paper: '#fff',
    "default": '#fff',
    neutral: GREY[200]
  },
  chart: CHART_COLORS,
  common: {
    black: '#000',
    white: '#fff'
  },
  divider: GREY['500_24'],
  error: _extends({}, ERROR),
  primary: _extends({}, PRIMARY),
  gradients: GRADIENTS,
  grey: GREY,
  info: _extends({}, INFO),
  secondary: _extends({}, SECONDARY),
  success: _extends({}, SUCCESS),
  text: {
    primary: GREY[800],
    secondary: GREY[600],
    disabled: GREY[500]
  },
  warning: _extends({}, WARNING)
};

var LIGHT_MODE = palette.grey[500];

var createShadow = function createShadow(color) {
  var transparent1 = alpha(color, 0.2);
  var transparent2 = alpha(color, 0.14);
  var transparent3 = alpha(color, 0.12);
  return ['none', "0px 2px 1px -1px " + transparent1 + ",0px 1px 1px 0px " + transparent2 + ",0px 1px 3px 0px " + transparent3, "0px 3px 1px -2px " + transparent1 + ",0px 2px 2px 0px " + transparent2 + ",0px 1px 5px 0px " + transparent3, "0px 3px 3px -2px " + transparent1 + ",0px 3px 4px 0px " + transparent2 + ",0px 1px 8px 0px " + transparent3, "0px 2px 4px -1px " + transparent1 + ",0px 4px 5px 0px " + transparent2 + ",0px 1px 10px 0px " + transparent3, "0px 3px 5px -1px " + transparent1 + ",0px 5px 8px 0px " + transparent2 + ",0px 1px 14px 0px " + transparent3, "0px 3px 5px -1px " + transparent1 + ",0px 6px 10px 0px " + transparent2 + ",0px 1px 18px 0px " + transparent3, "0px 4px 5px -2px " + transparent1 + ",0px 7px 10px 1px " + transparent2 + ",0px 2px 16px 1px " + transparent3, "0px 5px 5px -3px " + transparent1 + ",0px 8px 10px 1px " + transparent2 + ",0px 3px 14px 2px " + transparent3, "0px 5px 6px -3px " + transparent1 + ",0px 9px 12px 1px " + transparent2 + ",0px 3px 16px 2px " + transparent3, "0px 6px 6px -3px " + transparent1 + ",0px 10px 14px 1px " + transparent2 + ",0px 4px 18px 3px " + transparent3, "0px 6px 7px -4px " + transparent1 + ",0px 11px 15px 1px " + transparent2 + ",0px 4px 20px 3px " + transparent3, "0px 7px 8px -4px " + transparent1 + ",0px 12px 17px 2px " + transparent2 + ",0px 5px 22px 4px " + transparent3, "0px 7px 8px -4px " + transparent1 + ",0px 13px 19px 2px " + transparent2 + ",0px 5px 24px 4px " + transparent3, "0px 7px 9px -4px " + transparent1 + ",0px 14px 21px 2px " + transparent2 + ",0px 5px 26px 4px " + transparent3, "0px 8px 9px -5px " + transparent1 + ",0px 15px 22px 2px " + transparent2 + ",0px 6px 28px 5px " + transparent3, "0px 8px 10px -5px " + transparent1 + ",0px 16px 24px 2px " + transparent2 + ",0px 6px 30px 5px " + transparent3, "0px 8px 11px -5px " + transparent1 + ",0px 17px 26px 2px " + transparent2 + ",0px 6px 32px 5px " + transparent3, "0px 9px 11px -5px " + transparent1 + ",0px 18px 28px 2px " + transparent2 + ",0px 7px 34px 6px " + transparent3, "0px 9px 12px -6px " + transparent1 + ",0px 19px 29px 2px " + transparent2 + ",0px 7px 36px 6px " + transparent3, "0px 10px 13px -6px " + transparent1 + ",0px 20px 31px 3px " + transparent2 + ",0px 8px 38px 7px " + transparent3, "0px 10px 13px -6px " + transparent1 + ",0px 21px 33px 3px " + transparent2 + ",0px 8px 40px 7px " + transparent3, "0px 10px 14px -6px " + transparent1 + ",0px 22px 35px 3px " + transparent2 + ",0px 8px 42px 7px " + transparent3, "0px 11px 14px -7px " + transparent1 + ",0px 23px 36px 3px " + transparent2 + ",0px 9px 44px 8px " + transparent3, "0px 11px 15px -7px " + transparent1 + ",0px 24px 38px 3px " + transparent2 + ",0px 9px 46px 8px " + transparent3];
};

var createCustomShadow = function createCustomShadow(color) {
  var transparent = alpha(color, 0.24);
  return {
    z1: "0 1px 2px 0 " + transparent,
    z8: "0 8px 16px 0 " + transparent,
    z12: "0 0 2px 0 " + transparent + ", 0 12px 24px 0 " + transparent,
    z16: "0 0 2px 0 " + transparent + ", 0 16px 32px -4px " + transparent,
    z20: "0 0 2px 0 " + transparent + ", 0 20px 40px -4px " + transparent,
    z24: "0 0 4px 0 " + transparent + ", 0 24px 48px 0 " + transparent,
    primary: "0 8px 16px 0 " + alpha(palette.primary.main, 0.24),
    secondary: "0 8px 16px 0 " + alpha(palette.secondary.main, 0.24),
    info: "0 8px 16px 0 " + alpha(palette.info.main, 0.24),
    success: "0 8px 16px 0 " + alpha(palette.success.main, 0.24),
    warning: "0 8px 16px 0 " + alpha(palette.warning.main, 0.24),
    error: "0 8px 16px 0 " + alpha(palette.error.main, 0.24)
  };
};

var customShadows = createCustomShadow(LIGHT_MODE);
var shadows = createShadow(LIGHT_MODE);

var shape = {
  borderRadius: 8,
  borderRadiusSm: 12,
  borderRadiusMd: 16
};

var pxToRem = function pxToRem(value) {
  return value / 16 + "rem";
};
var responsiveFontSizes = function responsiveFontSizes(_ref) {
  var sm = _ref.sm,
      md = _ref.md,
      lg = _ref.lg;
  return {
    '@media (min-width:600px)': {
      fontSize: pxToRem(sm)
    },
    '@media (min-width:900px)': {
      fontSize: pxToRem(md)
    },
    '@media (min-width:1200px)': {
      fontSize: pxToRem(lg)
    }
  };
};
var FONT_PRIMARY = 'Montserrat, sans-serif';
var typography = {
  fontFamily: FONT_PRIMARY,
  fontWeightRegular: 400,
  fontWeightMedium: 600,
  fontWeightBold: 700,
  h1: _extends({
    fontWeight: 700,
    lineHeight: 80 / 64,
    fontSize: pxToRem(40)
  }, responsiveFontSizes({
    sm: 52,
    md: 58,
    lg: 64
  })),
  h2: _extends({
    fontWeight: 700,
    lineHeight: 64 / 48,
    fontSize: pxToRem(32)
  }, responsiveFontSizes({
    sm: 40,
    md: 44,
    lg: 48
  })),
  h3: _extends({
    fontWeight: 700,
    lineHeight: 1.5,
    fontSize: pxToRem(24)
  }, responsiveFontSizes({
    sm: 26,
    md: 30,
    lg: 32
  })),
  h4: _extends({
    fontWeight: 700,
    lineHeight: 1.5,
    fontSize: pxToRem(20)
  }, responsiveFontSizes({
    sm: 20,
    md: 24,
    lg: 24
  })),
  h5: _extends({
    fontWeight: 700,
    lineHeight: 1.5,
    fontSize: pxToRem(18)
  }, responsiveFontSizes({
    sm: 19,
    md: 20,
    lg: 20
  })),
  h6: _extends({
    fontWeight: 700,
    lineHeight: 28 / 18,
    fontSize: pxToRem(17)
  }, responsiveFontSizes({
    sm: 18,
    md: 18,
    lg: 18
  })),
  subtitle1: {
    fontWeight: 600,
    lineHeight: 1.5,
    fontSize: pxToRem(16)
  },
  subtitle2: {
    fontWeight: 600,
    lineHeight: 22 / 14,
    fontSize: pxToRem(14)
  },
  body1: {
    lineHeight: 1.5,
    fontSize: pxToRem(16)
  },
  body2: {
    lineHeight: 22 / 14,
    fontSize: pxToRem(14)
  },
  caption: {
    lineHeight: 1.5,
    fontSize: pxToRem(12)
  },
  overline: {
    fontWeight: 700,
    lineHeight: 1.5,
    fontSize: pxToRem(12),
    letterSpacing: 1.1,
    textTransform: 'uppercase'
  },
  button: {
    fontWeight: 700,
    lineHeight: 24 / 14,
    fontSize: pxToRem(14),
    textTransform: 'capitalize'
  }
};

var ThemeConfig = function ThemeConfig(_ref) {
  var children = _ref.children;
  var themeOptions = {
    breakpoints: breakpoints,
    customShadows: customShadows,
    palette: palette,
    shadows: shadows,
    shape: shape,
    typography: typography
  };
  var theme = createTheme(themeOptions);
  theme.components = ComponentsOverrides(theme);
  return /*#__PURE__*/React.createElement(StyledEngineProvider, {
    injectFirst: true
  }, /*#__PURE__*/React.createElement(ThemeProvider, {
    theme: theme
  }, /*#__PURE__*/React.createElement(CssBaseline, null), children));
};

ThemeConfig.propTypes = {
  children: propTypes.node
};

var _excluded$3 = ["children", "onClose"];
var BootstrapDialog = styled(Dialog$1)(function (_ref) {
  var theme = _ref.theme;
  return {
    '& .MuiDialogContent-root': {
      padding: theme.spacing(2)
    },
    '& .MuiDialogActions-root': {
      padding: theme.spacing(1)
    }
  };
});

var BootstrapDialogTitle = function BootstrapDialogTitle(props) {
  var children = props.children,
      onClose = props.onClose,
      other = _objectWithoutPropertiesLoose(props, _excluded$3);

  return /*#__PURE__*/React.createElement(DialogTitle, _extends({
    sx: {
      m: 0,
      p: 2
    }
  }, other), children, onClose ? /*#__PURE__*/React.createElement(IconButton$1, {
    "aria-label": "close",
    onClick: onClose,
    sx: {
      position: 'absolute',
      right: 8,
      top: 8,
      color: function color(theme) {
        return theme.palette.grey[500];
      }
    }
  }, /*#__PURE__*/React.createElement(CloseIcon, null)) : null);
};

BootstrapDialogTitle.propTypes = {
  children: propTypes.node,
  onClose: propTypes.func.isRequired
};

var StyledDialog = function StyledDialog(_ref2) {
  var children = _ref2.children,
      _ref2$disableEscapeKe = _ref2.disableEscapeKeyDown,
      disableEscapeKeyDown = _ref2$disableEscapeKe === void 0 ? false : _ref2$disableEscapeKe,
      _ref2$footerComponent = _ref2.footerComponent,
      FooterComponent = _ref2$footerComponent === void 0 ? null : _ref2$footerComponent,
      _ref2$onClose = _ref2.onClose,
      onClose = _ref2$onClose === void 0 ? function () {} : _ref2$onClose,
      _ref2$onEnter = _ref2.onEnter,
      onEnter = _ref2$onEnter === void 0 ? function () {} : _ref2$onEnter,
      _ref2$onExited = _ref2.onExited,
      onExited = _ref2$onExited === void 0 ? function () {} : _ref2$onExited,
      _ref2$open = _ref2.open,
      open = _ref2$open === void 0 ? false : _ref2$open,
      _ref2$scroll = _ref2.scroll,
      scroll = _ref2$scroll === void 0 ? 'body' : _ref2$scroll,
      _ref2$size = _ref2.size,
      size = _ref2$size === void 0 ? 'lg' : _ref2$size,
      title = _ref2.title;
  return /*#__PURE__*/React.createElement(BootstrapDialog, {
    disableEscapeKeyDown: disableEscapeKeyDown,
    fullWidth: true,
    maxWidth: size,
    onClose: onClose,
    open: open === null ? false : open,
    scroll: scroll,
    TransitionProps: {
      onEnter: onEnter,
      onExited: onExited
    }
  }, /*#__PURE__*/React.createElement(BootstrapDialogTitle, {
    onClose: onClose,
    sx: {
      minHeight: '60px'
    }
  }, title), /*#__PURE__*/React.createElement(DialogContent, {
    dividers: true
  }, /*#__PURE__*/React.createElement(Box, null, children)), FooterComponent && /*#__PURE__*/React.createElement(DialogActions, null, /*#__PURE__*/React.createElement(FooterComponent, null)));
};

StyledDialog.propTypes = {
  children: propTypes.oneOfType([propTypes.object, propTypes.array]),
  disableEscapeKeyDown: propTypes.bool,
  footerComponent: propTypes.oneOfType([propTypes.object, propTypes.func]),
  onClose: propTypes.func,
  onEnter: propTypes.func,
  onExited: propTypes.func,
  open: propTypes.bool,
  scroll: propTypes.string,
  size: propTypes.oneOfType([propTypes.string, propTypes.bool]),
  title: propTypes.oneOfType([propTypes.string, propTypes.object])
};

var TRANSLATIONS_EN = {
	
};

var TRANSLATIONS_ES = {
	
};

var TRANSLATIONS_PT = {
	
};

i18n.use(LanguageDetector).use(initReactI18next).init({
  fallbackLng: 'en',
  interpolation: {
    escapeValue: false
  },
  keySeparator: false,
  react: {
    transSupportBasicHtmlNodes: true,
    useSuspense: true
  },
  resources: {
    en: {
      translation: TRANSLATIONS_EN
    },
    es: {
      translation: TRANSLATIONS_ES
    },
    pt: {
      translation: TRANSLATIONS_PT
    }
  }
});

export { BottomActions, ButtonComponentForwardRef as Button, _Card as Card, ConfirmDialog, CustomPagination, StackItem, TabPanel, ThemeConfig, StyledDialog as UiDialog };
//# sourceMappingURL=index.modern.js.map
