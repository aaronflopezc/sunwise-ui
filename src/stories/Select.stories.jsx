import React from 'react';

import {
    Checkbox,
    FormControl,
    FormHelperText,
    InputLabel,
    ListItemText,
    ListSubheader,
    MenuItem,
    OutlinedInput,
    Select,
} from '../.';

export default {
    title: 'Components/Inputs/Select',
    component: Select,
    argTypes: {
        color: {
            control: 'select',
            description: '',
            options: [
                'error',
                'info',
                'primary',
                'secondary',
                'success',
                'warning',
            ],
            table: {
                defaultValue: {
                    summary: 'primary',
                },
            },
        },
        disabled: {
            control: 'boolean',
            defaultValue: false,
        },
        error: {
            control: 'boolean',
            defaultValue: false,
        },
        required: {
            control: 'boolean',
            defaultValue: false,
        },
        readOnly: {
            control: 'boolean',
            defaultValue: false,
        },
        size: {
            control: 'select',
            options: ['small', 'medium'],
            table: {
                defaultValue: {
                    summary: 'medium',
                },
            },
        },
    },
};

const Basic = ({ ...args }) => {
    const [age, setAge] = React.useState('');

    const handleChange = (event) => {
        setAge(event.target.value);
    };

    return (
        <FormControl
            fullWidth
            disabled={args.disabled}
            color={args.color}
            size={args.size}
            error={args.error}
            required={args.required}
        >
            <InputLabel>Age</InputLabel>
            <Select
                value={age}
                label="Age"
                onChange={handleChange}
                inputProps={{ readOnly: args.readOnly }}
            >
                <MenuItem value={10}>Ten</MenuItem>
                <MenuItem value={20}>Twenty</MenuItem>
                <MenuItem value={30}>Thirty</MenuItem>
            </Select>
            <FormHelperText>Helper text (optional)</FormHelperText>
        </FormControl>
    );
};

const MultipleTemplate = ({ ...args }) => {
    const [age, setAge] = React.useState([]);

    const handleChange = (event) => {
        setAge(event.target.value);
    };

    return (
        <FormControl
            fullWidth
            disabled={args.disabled}
            color={args.color}
            size={args.size}
            error={args.error}
            required={args.required}
        >
            <InputLabel>Age</InputLabel>
            <Select
                multiple
                value={age}
                label="Age"
                onChange={handleChange}
                inputProps={{ readOnly: args.readOnly }}
            >
                <MenuItem value={10}>Ten</MenuItem>
                <MenuItem value={20}>Twenty</MenuItem>
                <MenuItem value={30}>Thirty</MenuItem>
            </Select>
            <FormHelperText>Helper text (optional)</FormHelperText>
        </FormControl>
    );
};

const CheckmarksTemplate = ({ ...args }) => {
    const names = [
        'Oliver Hansen',
        'Van Henry',
        'April Tucker',
        'Ralph Hubbard',
        'Omar Alexander',
        'Carlos Abbott',
        'Miriam Wagner',
        'Bradley Wilkerson',
        'Virginia Andrews',
        'Kelly Snyder',
    ];

    const ITEM_HEIGHT = 48;
    const ITEM_PADDING_TOP = 8;
    const MenuProps = {
        PaperProps: {
            style: {
                maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
                width: 250,
            },
        },
    };

    const [personName, setPersonName] = React.useState([]);

    const handleChange = (event) => {
        const {
            target: { value },
        } = event;
        setPersonName(typeof value === 'string' ? value.split(',') : value);
    };

    return (
        <FormControl
            fullWidth
            disabled={args.disabled}
            color={args.color}
            size={args.size}
            error={args.error}
            required={args.required}
            input={<OutlinedInput label="Tag" />}
            renderValue={(selected) => selected.join(', ')}
            MenuProps={MenuProps}
        >
            <InputLabel>Tag</InputLabel>
            <Select
                multiple
                value={personName}
                label="Age"
                onChange={handleChange}
                inputProps={{ readOnly: args.readOnly }}
                input={<OutlinedInput label="Tag" />}
                renderValue={(selected) => selected.join(', ')}
                MenuProps={MenuProps}
            >
                {names.map((name) => (
                    <MenuItem key={name} value={name}>
                        <Checkbox checked={personName.indexOf(name) > -1} />
                        <ListItemText primary={name} />
                    </MenuItem>
                ))}
            </Select>
            <FormHelperText>Helper text (optional)</FormHelperText>
        </FormControl>
    );
};

const GroupingTemplate = ({ ...args }) => (
    <FormControl
        fullWidth
        disabled={args.disabled}
        color={args.color}
        size={args.size}
        error={args.error}
        required={args.required}
    >
        <InputLabel>Grouping</InputLabel>
        <Select defaultValue="" inputProps={{ readOnly: args.readOnly }}>
            <MenuItem value="">
                <em>None</em>
            </MenuItem>
            <ListSubheader>Category 1</ListSubheader>
            <MenuItem value={1}>Option 1</MenuItem>
            <MenuItem value={2}>Option 2</MenuItem>
            <ListSubheader>Category 2</ListSubheader>
            <MenuItem value={3}>Option 3</MenuItem>
            <MenuItem value={4}>Option 4</MenuItem>
        </Select>
        <FormHelperText>Helper text (optional)</FormHelperText>
    </FormControl>
);

export const basic = Basic.bind({});
export const multipleSelect = MultipleTemplate.bind({});
export const checkmarks = CheckmarksTemplate.bind({});
export const grouping = GroupingTemplate.bind({});
