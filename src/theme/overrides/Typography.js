const Typography = (theme) => ({
    MuiTypography: {
        styleOverrides: {
            gutterBottom: { marginBottom: theme.spacing(2) },
            paragraph: { marginBottom: theme.spacing(2) },
        },
    },
});

export default Typography;
