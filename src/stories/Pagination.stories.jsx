import React from 'react';

import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';

import { Pagination, PaginationItem, Stack } from '../.';

export default {
    title: 'Components/Navigation/Pagination',
    component: Pagination,
    argTypes: {
        color: {
            control: 'select',
            options: [
                'error',
                'info',
                'primary',
                'secondary',
                'success',
                'warning',
            ],
            table: { defaultValue: { summary: 'primary' } },
        },
        disabled: {
            control: 'boolean',
        },
        showFirstButton: {
            control: 'boolean',
        },
        showLastButton: {
            control: 'boolean',
        },
        hidePrevButton: {
            control: 'boolean',
        },
        hideNextButton: {
            control: 'boolean',
        },
        shape: {
            control: 'select',
            options: ['rounded', 'circular'],
            table: { defaultValue: { summary: 'circular' } },
        },
        size: {
            control: 'select',
            options: ['small', 'medium', 'large'],
            table: { defaultValue: { summary: 'medium' } },
        },
        variant: {
            control: 'select',
            options: ['text', 'outlined'],
            table: { defaultValue: { summary: 'text' } },
        },
    },
};

const Basic = ({ ...args }) => <Pagination count={10} {...args} />;

const CustomIcons = ({ ...args }) => (
    <Pagination
        count={10}
        {...args}
        renderItem={(item) => (
            <PaginationItem
                components={{ previous: ArrowBackIcon, next: ArrowForwardIcon }}
                {...item}
            />
        )}
    />
);

const PaginationRanges = ({ ...args }) => (
    <Stack spacing={2}>
        <Pagination {...args} count={11} defaultPage={6} siblingCount={0} />
        <Pagination {...args} count={11} defaultPage={6} />
        <Pagination
            {...args}
            count={11}
            defaultPage={6}
            siblingCount={0}
            boundaryCount={2}
        />
        <Pagination {...args} count={11} defaultPage={6} boundaryCount={2} />
    </Stack>
);

export const basic = Basic.bind({});
export const customIcons = CustomIcons.bind({});
export const paginationRanges = PaginationRanges.bind({});
