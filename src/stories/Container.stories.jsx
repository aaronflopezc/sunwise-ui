import React from 'react';

import { Box, Container } from '../.';

export default {
    title: 'Components/Layout/Container',
    component: Container,
    argTypes: {
        disableGutters: {
            control: 'boolean',
            defaultValue: false,
        },
        maxWidth: {
            control: 'select',
            options: ['xs', 'sm', 'md', 'lg', 'xl', false],
        },
        fixed: {
            control: 'boolean',
            defaultValue: false,
        },
    },
};

const Basic = ({ ...args }) => (
    <Container {...args}>
        <Box sx={{ bgcolor: '#cfe8fc', height: '500px' }} />
    </Container>
);
export const basic = Basic.bind({});
