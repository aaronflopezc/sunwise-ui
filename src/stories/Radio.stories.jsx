import React from 'react';

import PropTypes from 'prop-types';

import {
    Radio,
    RadioGroup,
    FormControl,
    FormLabel,
    FormControlLabel,
} from '../.';

export default {
    title: 'Components/Inputs/Radio',
    component: Radio,
    argTypes: {
        color: {
            control: 'select',
            description: '',
            options: [
                'error',
                'info',
                'primary',
                'secondary',
                'success',
                'warning',
            ],
            table: {
                defaultValue: {
                    summary: 'primary',
                },
            },
        },
        disabled: {
            control: 'boolean',
            defaultValue: false,
        },
        row: {
            control: 'boolean',
            defaultValue: false,
        },
        size: {
            control: 'select',
            options: ['small', 'medium'],
            table: {
                defaultValue: {
                    summary: 'medium',
                },
            },
        },
    },
};

const Basic = ({ ...args }) => {
    const [selectedValue, setSelectedValue] = React.useState('a');
    const handleChange = (event) => setSelectedValue(event.target.value);

    return (
        <div>
            <Radio
                {...args}
                checked={selectedValue === 'a'}
                onChange={handleChange}
                value="a"
                name="radio-buttons"
                inputProps={{ 'aria-label': 'A' }}
            />
            <Radio
                {...args}
                checked={selectedValue === 'b'}
                onChange={handleChange}
                value="b"
                name="radio-buttons"
                inputProps={{ 'aria-label': 'B' }}
            />
        </div>
    );
};

const RadioGroupTemplate = ({ row, ...args }) => (
    <FormControl color={args.color} disabled={args.disabled}>
        <FormLabel>Gender</FormLabel>
        <RadioGroup row={row} defaultValue="female" name="radio-buttons-group">
            <FormControlLabel
                value="female"
                control={<Radio {...args} />}
                label="Female"
            />
            <FormControlLabel
                value="male"
                control={<Radio {...args} />}
                label="Male"
            />
            <FormControlLabel
                value="other"
                control={<Radio {...args} />}
                label="Other"
            />
        </RadioGroup>
    </FormControl>
);
RadioGroupTemplate.propTypes = {
    row: PropTypes.bool,
};

const LabelPlacementTemplate = ({ row, ...args }) => (
    <FormControl color={args.color} disabled={args.disabled}>
        <RadioGroup row={row} name="position" defaultValue="top">
            <FormControlLabel
                value="top"
                control={<Radio {...args} />}
                label="Top"
                labelPlacement="top"
            />
            <FormControlLabel
                value="start"
                control={<Radio {...args} />}
                label="Start"
                labelPlacement="start"
            />
            <FormControlLabel
                value="bottom"
                control={<Radio {...args} />}
                label="Bottom"
                labelPlacement="bottom"
            />
            <FormControlLabel
                value="end"
                control={<Radio {...args} />}
                label="End"
            />
        </RadioGroup>
    </FormControl>
);
LabelPlacementTemplate.propTypes = {
    row: PropTypes.bool,
};

export const basic = Basic.bind({});
basic.parameters = {
    controls: { exclude: ['row'] },
};
export const group = RadioGroupTemplate.bind({});
export const labelPlacement = LabelPlacementTemplate.bind({});
