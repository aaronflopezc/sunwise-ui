import React, { forwardRef } from 'react';

import Button from '@mui/material/Button';
import PropTypes from 'prop-types';

const ButtonComponentForwardRef = forwardRef((props, ref) => {
    const { children, dataIntercomTarget, visible = true, ...rest } = props;

    if (!visible) return null;

    const innerProps = {
        ...rest,
        'data-intercom-target': dataIntercomTarget,
    };

    return (
        <Button {...innerProps} ref={ref}>
            {children}
        </Button>
    );
});

ButtonComponentForwardRef.propTypes = {
    children: PropTypes.node,
    dataIntercomTarget: PropTypes.string,
    visible: PropTypes.bool,
};

ButtonComponentForwardRef.displayName = 'Button';

export default ButtonComponentForwardRef;
