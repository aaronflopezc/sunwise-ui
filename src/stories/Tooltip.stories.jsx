import React from 'react';

import DeleteIcon from '@mui/icons-material/Delete';
import IconButton from '@mui/material/IconButton';

import { Box, Button, ClickAwayListener, Grid, Tooltip } from '../.';

export default {
    title: 'Components/Data Display/Tooltip',
    component: Tooltip,
    argTypes: {
        disabled: { control: 'boolean' },
        followCursor: { control: 'boolean' },
        placement: {
            control: 'select',
            options: [
                'top-start',
                'top',
                'top-end',
                'left-start',
                'left',
                'left-end',
                'right-start',
                'right',
                'right-end',
                'bottom-start',
                'bottom',
                'bottom-end',
            ],
        },
    },
};

const Basic = ({ ...args }) => (
    <Box sx={{ p: 10 }}>
        <Tooltip title="Delete" {...args}>
            <IconButton>
                <DeleteIcon />
            </IconButton>
        </Tooltip>
    </Box>
);

const TriggersTooltips = ({ ...args }) => {
    const [open, setOpen] = React.useState(false);
    const handleTooltipClose = () => setOpen(false);
    const handleTooltipOpen = () => setOpen(true);

    return (
        <div>
            <Grid container justifyContent="center">
                <Grid item>
                    <Tooltip {...args} disableFocusListener title="Add">
                        <Button>Hover or touch</Button>
                    </Tooltip>
                </Grid>
                <Grid item>
                    <Tooltip {...args} disableHoverListener title="Add">
                        <Button>Focus or touch</Button>
                    </Tooltip>
                </Grid>
                <Grid item>
                    <Tooltip
                        {...args}
                        disableFocusListener
                        disableTouchListener
                        title="Add"
                    >
                        <Button>Hover</Button>
                    </Tooltip>
                </Grid>
                <Grid item>
                    <ClickAwayListener onClickAway={handleTooltipClose}>
                        <div>
                            <Tooltip
                                {...args}
                                PopperProps={{
                                    disablePortal: true,
                                }}
                                onClose={handleTooltipClose}
                                open={open}
                                disableFocusListener
                                disableHoverListener
                                disableTouchListener
                                title="Add"
                            >
                                <Button onClick={handleTooltipOpen}>
                                    Click
                                </Button>
                            </Tooltip>
                        </div>
                    </ClickAwayListener>
                </Grid>
            </Grid>
        </div>
    );
};

export const basic = Basic.bind({});
export const triggersTooltips = TriggersTooltips.bind({});
