import { alpha } from '@mui/material/styles';

const createGradient = (color1, color2) =>
    `linear-gradient(to bottom, ${color1}, ${color2})`;
const GREY = {
    0: '#FFFFFF',
    100: '#F9FAFB',
    200: '#F4F6F8',
    300: '#DFE3E8',
    400: '#C4CDD5',
    500: '#919EAB',
    600: '#637381',
    700: '#454F5B',
    800: '#212B36',
    900: '#161C24',
    '500_8': alpha('#919EAB', 0.08),
    '500_12': alpha('#919EAB', 0.12),
    '500_16': alpha('#919EAB', 0.16),
    '500_24': alpha('#919EAB', 0.24),
    '500_32': alpha('#919EAB', 0.32),
    '500_48': alpha('#919EAB', 0.48),
    '500_56': alpha('#919EAB', 0.56),
    '500_80': alpha('#919EAB', 0.8),
};
const PRIMARY = {
    contrastText: '#fff',
    dark: '#A85B00',
    darker: '#C66B00',
    light: '#FFB843',
    lighter: '#FFCB48',
    main: '#FF9A00',
};
const SECONDARY = {
    contrastText: '#fff',
    dark: '#1939B7',
    darker: '#091A7A',
    light: '#84A9FF',
    lighter: '#D6E4FF',
    main: '#3366FF',
};
const INFO = {
    contrastText: '#fff',
    dark: '#0C53B7',
    darker: '#04297A',
    light: '#74CAFF',
    lighter: '#D0F2FF',
    main: '#1890FF',
};
const SUCCESS = {
    contrastText: GREY[800],
    dark: '#229A16',
    darker: '#08660D',
    light: '#AAF27F',
    lighter: '#E9FCD4',
    main: '#54D62C',
};
const WARNING = {
    contrastText: GREY[800],
    dark: '#B78103',
    darker: '#7A4F01',
    light: '#FFE16A',
    lighter: '#FFF7CD',
    main: '#FFC107',
};
const ERROR = {
    contrastText: '#fff',
    dark: '#B72136',
    darker: '#7A0C2E',
    light: '#FFA48D',
    lighter: '#FFE7D9',
    main: '#FF4842',
};
const GRADIENTS = {
    error: createGradient(ERROR.light, ERROR.main),
    info: createGradient(INFO.light, INFO.main),
    primary: createGradient(PRIMARY.light, PRIMARY.main),
    success: createGradient(SUCCESS.light, SUCCESS.main),
    warning: createGradient(WARNING.light, WARNING.main),
};
const CHART_COLORS = {
    blue: ['#2D99FF', '#83CFFF', '#A5F3FF', '#CCFAFF'],
    green: ['#2CD9C5', '#60F1C8', '#A4F7CC', '#C0F2DC'],
    red: ['#FF6C40', '#FF8F6D', '#FFBD98', '#FFF2D4'],
    violet: ['#826AF9', '#9E86FF', '#D0AEFF', '#F7D2FF'],
    yellow: ['#FFE700', '#FFEF5A', '#FFF7AE', '#FFF3D6'],
};
const palette = {
    action: {
        active: GREY[600],
        hover: GREY['500_8'],
        selected: GREY['500_16'],
        disabled: GREY['500_80'],
        disabledBackground: GREY['500_24'],
        focus: GREY['500_24'],
        hoverOpacity: 0.08,
        disabledOpacity: 0.48,
    },
    background: { paper: '#fff', default: '#fff', neutral: GREY[200] },
    chart: CHART_COLORS,
    common: { black: '#000', white: '#fff' },
    divider: GREY['500_24'],
    error: { ...ERROR },
    primary: { ...PRIMARY },
    gradients: GRADIENTS,
    grey: GREY,
    info: { ...INFO },
    secondary: { ...SECONDARY },
    success: { ...SUCCESS },
    text: { primary: GREY[800], secondary: GREY[600], disabled: GREY[500] },
    warning: { ...WARNING },
};

export default palette;
