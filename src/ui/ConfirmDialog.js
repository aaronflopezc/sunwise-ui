import React from 'react';

import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';

import Button from './Button';

const ConfirmDialog = ({
    handleClickClose,
    handleClickSuccess,
    isOpen = false,
    message,
    title,
}) => {
    const { t } = useTranslation();
    return (
        <Dialog
            open={isOpen === null ? false : isOpen}
            onClose={handleClickClose}
        >
            <DialogTitle>{title}</DialogTitle>
            <DialogContent dividers>
                <h6>{message}</h6>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClickClose} color="error">
                    <strong>{t('Cancel').toUpperCase()}</strong>
                </Button>
                <Button onClick={handleClickSuccess}>
                    <strong>{t('Accept').toUpperCase()}</strong>
                </Button>
            </DialogActions>
        </Dialog>
    );
};

ConfirmDialog.propTypes = {
    handleClickClose: PropTypes.func,
    handleClickSuccess: PropTypes.func,
    isOpen: PropTypes.bool,
    message: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    title: PropTypes.string,
};

export default ConfirmDialog;
