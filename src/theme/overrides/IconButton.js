const IconButton = (theme) => ({
    MuiIconButton: {
        variants: [
            {
                props: { color: 'default' },
                style: {
                    '&:hover': {
                        backgroundColor: theme.palette.action.hover,
                    },
                },
            },
            {
                props: { color: 'inherit' },
                style: {
                    '&:hover': {
                        backgroundColor: theme.palette.action.hover,
                    },
                },
            },
        ],
    },
});

export default IconButton;
