import React from 'react';

import { styled } from '@mui/material/styles';
import PropTypes from 'prop-types';

import { FormControl, FormGroup, FormControlLabel, Switch } from '../.';

export default {
    title: 'Components/Inputs/Switch',
    component: Switch,
    argTypes: {
        checked: {
            control: 'boolean',
        },
        color: {
            control: 'select',
            description: '',
            options: [
                'error',
                'info',
                'primary',
                'secondary',
                'success',
                'warning',
            ],
            table: {
                defaultValue: {
                    summary: 'primary',
                },
            },
        },
        disabled: {
            control: 'boolean',
        },
        row: {
            control: 'boolean',
            defaultValue: false,
        },
        size: {
            control: 'select',
            options: ['small', 'medium'],
            table: {
                defaultValue: {
                    summary: 'medium',
                },
            },
        },
    },
};

const Basic = ({ row, ...args }) => {
    return (
        <FormGroup row={row}>
            <Switch {...args} defaultChecked />
            <Switch {...args} />
            <Switch {...args} disabled defaultChecked />
            <Switch {...args} disabled />
        </FormGroup>
    );
};
Basic.propTypes = {
    row: PropTypes.bool,
};

const Label = ({ row, ...args }) => {
    return (
        <FormGroup row={row}>
            <FormControlLabel
                control={<Switch {...args} defaultChecked />}
                label="Label"
            />
            <FormControlLabel disabled control={<Switch />} label="Disabled" />
        </FormGroup>
    );
};
Label.propTypes = {
    row: PropTypes.bool,
};

const LabelPlacement = ({ row, ...args }) => {
    return (
        <FormControl component="fieldset">
            <FormGroup row={row}>
                <FormControlLabel
                    value="top"
                    control={<Switch {...args} />}
                    label="Top"
                    labelPlacement="top"
                />
                <FormControlLabel
                    value="start"
                    control={<Switch {...args} />}
                    label="Start"
                    labelPlacement="start"
                />
                <FormControlLabel
                    value="bottom"
                    control={<Switch {...args} />}
                    label="Bottom"
                    labelPlacement="bottom"
                />
                <FormControlLabel
                    value="end"
                    control={<Switch {...args} />}
                    label="End"
                    labelPlacement="end"
                />
            </FormGroup>
        </FormControl>
    );
};
LabelPlacement.propTypes = {
    row: PropTypes.bool,
};

const Customization = ({ ...args }) => {
    const Android12Switch = styled(Switch)(({ theme }) => ({
        padding: 8,
        '& .MuiSwitch-track': {
            borderRadius: 22 / 2,
            '&:before, &:after': {
                content: '""',
                position: 'absolute',
                top: '50%',
                transform: 'translateY(-50%)',
                width: 16,
                height: 16,
            },
            '&:before': {
                backgroundImage: `url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" height="16" width="16" viewBox="0 0 24 24"><path fill="${encodeURIComponent(
                    theme.palette.getContrastText(theme.palette.secondary.main)
                )}" d="M21,7L9,19L3.5,13.5L4.91,12.09L9,16.17L19.59,5.59L21,7Z"/></svg>')`,
                left: 12,
            },
            '&:after': {
                backgroundImage: `url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" height="16" width="16" viewBox="0 0 24 24"><path fill="${encodeURIComponent(
                    theme.palette.getContrastText(theme.palette.secondary.main)
                )}" d="M19,13H5V11H19V13Z" /></svg>')`,
                right: 12,
            },
        },
        '& .MuiSwitch-thumb': {
            boxShadow: 'none',
            width: 16,
            height: 16,
            margin: 2,
        },
    }));

    return (
        <FormControlLabel
            control={<Android12Switch {...args} defaultChecked />}
            label="Android 12"
        />
    );
};

export const basic = Basic.bind({});
export const label = Label.bind({});
export const labelPlacement = LabelPlacement.bind({});
export const customization = Customization.bind({});
customization.parameters = { controls: { exclude: ['row'] } };
