import React from 'react';

import { Skeleton, Stack } from '../.';

export default {
    title: 'Components/Feedback/Skeleton',
    component: Skeleton,
    argTypes: {
        animation: {
            control: 'select',
            description: '',
            options: [false, 'wave', 'pulse'],
        },
    },
};

const Basic = ({ ...args }) => (
    <Stack spacing={1}>
        <Skeleton {...args} variant="text" />
        <Skeleton {...args} variant="circular" width={40} height={40} />
        <Skeleton {...args} variant="rectangular" height={118} />
    </Stack>
);

const Animations = () => (
    <Stack spacing={1}>
        <Skeleton />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
    </Stack>
);

export const basic = Basic.bind({});
export const animations = Animations.bind({});
