import React from 'react';

import { Alert, Button, ThemeConfig } from 'sunwise-ui';

const App = () => {
    return (
        <ThemeConfig>
            <Button variant="contained">Ok!</Button>
            <Alert variant="filled" severity="warning" color="primary">
                Alert
            </Alert>
        </ThemeConfig>
    );
};

export default App;
