import React from 'react';

import AddIcon from '@mui/icons-material/Add';
import MailIcon from '@mui/icons-material/Mail';
import RemoveIcon from '@mui/icons-material/Remove';

import {
    Badge,
    Box,
    ButtonGroup,
    Button,
    FormControlLabel,
    Switch,
    Stack,
    FormControl,
    FormLabel,
    RadioGroup,
    Radio,
} from '../.';

export default {
    title: 'Components/Data Display/Badge',
    component: Badge,
    argTypes: {
        color: {
            control: 'select',
            description: '',
            options: [
                'error',
                'info',
                'primary',
                'secondary',
                'success',
                'warning',
            ],
        },
    },
};

const Basic = ({ ...args }) => (
    <Badge badgeContent={4} {...args}>
        <MailIcon color="action" />
    </Badge>
);

const Visibility = ({ ...args }) => {
    const [count, setCount] = React.useState(1);
    const [invisible, setInvisible] = React.useState(false);
    const handleBadgeVisibility = () => setInvisible(!invisible);

    return (
        <Box
            sx={{
                color: 'action.active',
                display: 'flex',
                flexDirection: 'column',
                '& > *': {
                    marginBottom: 2,
                },
                '& .MuiBadge-root': {
                    marginRight: 4,
                },
            }}
        >
            <div>
                <Badge {...args} badgeContent={count} max={9}>
                    <MailIcon />
                </Badge>
                <ButtonGroup>
                    <Button onClick={() => setCount(Math.max(count - 1, 0))}>
                        <RemoveIcon fontSize="small" />
                    </Button>
                    <Button onClick={() => setCount(count + 1)}>
                        <AddIcon fontSize="small" />
                    </Button>
                </ButtonGroup>
            </div>
            <div>
                <Badge {...args} variant="dot" invisible={invisible}>
                    <MailIcon />
                </Badge>
                <FormControlLabel
                    control={
                        <Switch
                            checked={!invisible}
                            onChange={handleBadgeVisibility}
                        />
                    }
                    label="Show Badge"
                />
            </div>
        </Box>
    );
};

const BadgeOverlap = ({ ...args }) => {
    const shapeStyles = { bgcolor: 'secondary.main', width: 40, height: 40 };
    const shapeCircleStyles = { borderRadius: '50%' };
    const rectangle = <Box component="span" sx={shapeStyles} />;
    const circle = (
        <Box component="span" sx={{ ...shapeStyles, ...shapeCircleStyles }} />
    );

    return (
        <Stack spacing={3} direction="row">
            <Badge {...args} badgeContent=" ">
                {rectangle}
            </Badge>
            <Badge {...args} badgeContent=" " variant="dot">
                {rectangle}
            </Badge>
            <Badge {...args} overlap="circular" badgeContent=" ">
                {circle}
            </Badge>
            <Badge {...args} overlap="circular" badgeContent=" " variant="dot">
                {circle}
            </Badge>
        </Stack>
    );
};

const BadgeAlignment = ({ ...args }) => {
    const [horizontal, setHorizontal] = React.useState('right');
    const [vertical, setVertical] = React.useState('top');

    return (
        <>
            <Box>
                <FormControl color={args.color}>
                    <FormLabel>Vertical</FormLabel>
                    <RadioGroup
                        name="controlled-radio-buttons-group-vertical"
                        value={vertical}
                        onChange={(event) => setVertical(event.target.value)}
                    >
                        <FormControlLabel
                            value="top"
                            control={<Radio color={args.color} />}
                            label="Top"
                        />
                        <FormControlLabel
                            value="bottom"
                            control={<Radio color={args.color} />}
                            label="Bottom"
                        />
                    </RadioGroup>
                </FormControl>

                <FormControl>
                    <FormLabel>Horizontal</FormLabel>
                    <RadioGroup
                        name="controlled-radio-buttons-group-horizontal"
                        value={horizontal}
                        onChange={(event) => setHorizontal(event.target.value)}
                    >
                        <FormControlLabel
                            value="right"
                            control={<Radio color={args.color} />}
                            label="Right"
                        />
                        <FormControlLabel
                            value="left"
                            control={<Radio color={args.color} />}
                            label="Left"
                        />
                    </RadioGroup>
                </FormControl>
            </Box>

            <Badge
                {...args}
                badgeContent={4}
                anchorOrigin={{
                    vertical,
                    horizontal,
                }}
            >
                <MailIcon color="action" />
            </Badge>
        </>
    );
};

export const basic = Basic.bind({});
export const visibility = Visibility.bind({});
export const badgeOverlap = BadgeOverlap.bind({});
badgeOverlap.args = { color: 'primary' };
export const badgeAlignment = BadgeAlignment.bind({});
badgeAlignment.args = { color: 'primary' };
