const Button = (theme) => ({
    MuiToggleButton: {
        styleOverrides: {
            root: {
                '&:hover': { boxShadow: 'none' },
                borderRadius: 20,
            },
        },
    },
    MuiButton: {
        defaultProps: {
            variant: 'contained',
        },
        styleOverrides: {
            root: {
                '&:hover': { boxShadow: 'none' },
                borderRadius: 20,
            },
            sizeLarge: { height: 48 },
            containedInherit: {
                '&:hover': {
                    backgroundColor: theme.palette.grey[400],
                },
                boxShadow: theme.customShadows.z8,
                color: theme.palette.grey[800],
            },
            containedPrimary: {
                boxShadow: theme.customShadows.primary,
            },
            containedSecondary: {
                boxShadow: theme.customShadows.secondary,
            },
            outlinedInherit: {
                '&:hover': {
                    backgroundColor: theme.palette.action.hover,
                },
                border: `1px solid ${theme.palette.grey['500_32']}`,
            },
            textInherit: {
                '&:hover': {
                    backgroundColor: theme.palette.action.hover,
                },
            },
        },
    },
});

export default Button;
