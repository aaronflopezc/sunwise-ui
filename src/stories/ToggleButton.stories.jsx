import React from 'react';

import { Stack, ToggleButton, ToggleButtonGroup } from '../.';

export default {
    title: 'Components/Inputs/ToggleButton',
    component: ToggleButton,
    argTypes: {
        color: {
            control: 'select',
            description: '',
            options: [
                'error',
                'info',
                'primary',
                'secondary',
                'success',
                'warning',
            ],
            table: {
                defaultValue: {
                    summary: 'primary',
                },
            },
        },
        fullWidth: {
            control: 'boolean',
        },
        disabled: {
            control: 'boolean',
        },
        size: {
            control: 'select',
            options: ['small', 'medium', 'large'],
            table: {
                defaultValue: {
                    summary: 'medium',
                },
            },
        },
        orientation: {
            control: 'select',
            options: ['horizontal', 'vertical'],
            table: {
                defaultValue: {
                    summary: 'horizontal',
                },
            },
        },
    },
};

const Basic = ({ ...args }) => {
    const [alignment, setAlignment] = React.useState('left');
    const [devices, setDevices] = React.useState(() => ['laptop']);

    const handleAlignment = (_, newAlignment) => {
        setAlignment(newAlignment);
    };

    const handleDevices = (_, newDevices) => {
        if (newDevices.length) {
            setDevices(newDevices);
        }
    };

    return (
        <Stack direction="row" spacing={4}>
            <ToggleButtonGroup
                value={alignment}
                exclusive
                onChange={handleAlignment}
                {...args}
            >
                <ToggleButton value="left">Single</ToggleButton>
                <ToggleButton value="center">Single</ToggleButton>
                <ToggleButton value="right">Single</ToggleButton>
                <ToggleButton value="justify" disabled>
                    Single
                </ToggleButton>
            </ToggleButtonGroup>

            <ToggleButtonGroup
                value={devices}
                onChange={handleDevices}
                {...args}
            >
                <ToggleButton value="laptop">Multi</ToggleButton>
                <ToggleButton value="tv">Multi</ToggleButton>
                <ToggleButton value="phone">Multi</ToggleButton>
            </ToggleButtonGroup>
        </Stack>
    );
};

export const basic = Basic.bind({});
