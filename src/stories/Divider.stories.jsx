import React from 'react';

import BeachAccessIcon from '@mui/icons-material/BeachAccess';
import FormatAlignCenterIcon from '@mui/icons-material/FormatAlignCenter';
import FormatAlignLeftIcon from '@mui/icons-material/FormatAlignLeft';
import FormatAlignRightIcon from '@mui/icons-material/FormatAlignRight';
import FormatBoldIcon from '@mui/icons-material/FormatBold';
import FormatItalicIcon from '@mui/icons-material/FormatItalic';
import ImageIcon from '@mui/icons-material/Image';
import WorkIcon from '@mui/icons-material/Work';
import { styled } from '@mui/material/styles';

import {
    Avatar,
    Box,
    Button,
    Chip,
    Divider,
    Grid,
    List,
    ListItem,
    ListItemAvatar,
    ListItemText,
    Stack,
    Typography,
} from '../.';

export default {
    title: 'Components/Data Display/Divider',
    component: Divider,
    argTypes: {
        children: { control: 'text' },
        light: {
            control: 'boolean',
        },
        orientation: {
            control: 'select',
            options: ['horizontal', 'vertical'],
            table: { defaultValue: { summary: 'horizontal' } },
        },
        textAlign: {
            control: 'select',
            options: ['center', 'left', 'right'],
            table: { defaultValue: { summary: 'center' } },
        },
        variant: {
            control: 'select',
            options: ['fullWidth', 'inset', 'middle'],
            table: { defaultValue: { summary: 'fullWidth' } },
        },
    },
};

const Basic = ({ ...args }) => <Divider {...args} />;

const ListDivider = () => (
    <List component="nav">
        <ListItem button>
            <ListItemText primary="Inbox" />
        </ListItem>
        <Divider />
        <ListItem button divider>
            <ListItemText primary="Drafts" />
        </ListItem>
        <ListItem button>
            <ListItemText primary="Trash" />
        </ListItem>
        <Divider light />
        <ListItem button>
            <ListItemText primary="Spam" />
        </ListItem>
    </List>
);

const InsetDividers = () => (
    <List
        sx={{
            width: '100%',
            maxWidth: 360,
            bgcolor: 'background.paper',
        }}
    >
        <ListItem>
            <ListItemAvatar>
                <Avatar>
                    <ImageIcon />
                </Avatar>
            </ListItemAvatar>
            <ListItemText primary="Photos" secondary="Jan 9, 2014" />
        </ListItem>
        <Divider variant="inset" component="li" />
        <ListItem>
            <ListItemAvatar>
                <Avatar>
                    <WorkIcon />
                </Avatar>
            </ListItemAvatar>
            <ListItemText primary="Work" secondary="Jan 7, 2014" />
        </ListItem>
        <Divider variant="inset" component="li" />
        <ListItem>
            <ListItemAvatar>
                <Avatar>
                    <BeachAccessIcon />
                </Avatar>
            </ListItemAvatar>
            <ListItemText primary="Vacation" secondary="July 20, 2014" />
        </ListItem>
    </List>
);

const SubheaderDividers = () => (
    <List
        sx={{
            width: '100%',
            maxWidth: 360,
            bgcolor: 'background.paper',
        }}
    >
        <ListItem>
            <ListItemText primary="Photos" secondary="Jan 9, 2014" />
        </ListItem>
        <Divider component="li" />
        <li>
            <Typography
                sx={{ mt: 0.5, ml: 2 }}
                color="text.secondary"
                display="block"
                variant="caption"
            >
                Divider
            </Typography>
        </li>
        <ListItem>
            <ListItemText primary="Work" secondary="Jan 7, 2014" />
        </ListItem>
        <Divider component="li" variant="inset" />
        <li>
            <Typography
                sx={{ mt: 0.5, ml: 9 }}
                color="text.secondary"
                display="block"
                variant="caption"
            >
                Leisure
            </Typography>
        </li>
        <ListItem>
            <ListItemAvatar>
                <Avatar>
                    <BeachAccessIcon />
                </Avatar>
            </ListItemAvatar>
            <ListItemText primary="Vacation" secondary="July 20, 2014" />
        </ListItem>
    </List>
);

const MiddleDivider = () => (
    <Box sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}>
        <Box sx={{ my: 3, mx: 2 }}>
            <Grid container alignItems="center">
                <Grid item xs>
                    <Typography gutterBottom variant="h4" component="div">
                        Toothbrush
                    </Typography>
                </Grid>
                <Grid item>
                    <Typography gutterBottom variant="h6" component="div">
                        $4.50
                    </Typography>
                </Grid>
            </Grid>
            <Typography color="text.secondary" variant="body2">
                Pinstriped cornflower blue cotton blouse takes you on a walk to
                the park or just down the hall.
            </Typography>
        </Box>
        <Divider variant="middle" />
        <Box sx={{ m: 2 }}>
            <Typography gutterBottom variant="body1">
                Select type
            </Typography>
            <Stack direction="row" spacing={1}>
                <Chip label="Extra Soft" />
                <Chip color="primary" label="Soft" />
                <Chip label="Medium" />
                <Chip label="Hard" />
            </Stack>
        </Box>
        <Box sx={{ mt: 3, ml: 1, mb: 1 }}>
            <Button>Add to cart</Button>
        </Box>
    </Box>
);

const DividersWithText = () => {
    const Root = styled('div')(({ theme }) => ({
        width: '100%',
        ...theme.typography.body2,
        '& > :not(style) + :not(style)': {
            marginTop: theme.spacing(2),
        },
    }));

    const content = (
        <div>
            {`Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus id dignissim justo.
     Nulla ut facilisis ligula. Interdum et malesuada fames ac ante ipsum primis in faucibus.
     Sed malesuada lobortis pretium.`}
        </div>
    );

    return (
        <Root>
            {content}
            <Divider>CENTER</Divider>
            {content}
            <Divider textAlign="left">LEFT</Divider>
            {content}
            <Divider textAlign="right">RIGHT</Divider>
            {content}
            <Divider>
                <Chip label="CHIP" />
            </Divider>
            {content}
        </Root>
    );
};

const VerticalDivider = ({ ...args }) => (
    <div>
        <Box
            sx={{
                display: 'flex',
                alignItems: 'center',
                width: 'fit-content',
                border: (theme) => `1px solid ${theme.palette.divider}`,
                borderRadius: 1,
                bgcolor: 'background.paper',
                color: 'text.secondary',
                '& svg': {
                    m: 1.5,
                },
                '& hr': {
                    mx: 0.5,
                },
            }}
        >
            <FormatAlignLeftIcon />
            <FormatAlignCenterIcon />
            <FormatAlignRightIcon />
            <Divider {...args} orientation="vertical" flexItem />
            <FormatBoldIcon />
            <FormatItalicIcon />
        </Box>
    </div>
);

const VerticalWithText = () => {
    const content = (
        <div>
            {`Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus id dignissim justo.
       Nulla ut facilisis ligula. Interdum et malesuada fames ac ante ipsum primis in faucibus.
       Sed malesuada lobortis pretium.`}
        </div>
    );

    return (
        <Grid container>
            <Grid item xs>
                {content}
            </Grid>
            <Divider orientation="vertical" flexItem>
                VERTICAL
            </Divider>
            <Grid item xs>
                {content}
            </Grid>
        </Grid>
    );
};

export const basic = Basic.bind({});
export const listDivider = ListDivider.bind({});
export const insetDividers = InsetDividers.bind({});
export const subheaderDividers = SubheaderDividers.bind({});
export const middleDivider = MiddleDivider.bind({});
export const dividersWithText = DividersWithText.bind({});
export const verticalDivider = VerticalDivider.bind({});
export const VerticalWithVariantMiddle = VerticalDivider.bind({});
VerticalWithVariantMiddle.args = {
    variant: 'middle',
};
export const verticalWithText = VerticalWithText.bind({});
