import React from 'react';

import { Button, Backdrop } from '../.';

export default {
    title: 'Components/Feedback/Backdrop',
    component: Backdrop,
};

const Basic = () => {
    const [open, setOpen] = React.useState(false);
    const handleClose = () => setOpen(false);
    const handleToggle = () => setOpen(!open);

    return (
        <div>
            <Button onClick={handleToggle}>Show backdrop</Button>
            <Backdrop
                sx={{
                    zIndex: (theme) => theme.zIndex.drawer + 1,
                }}
                open={open}
                onClick={handleClose}
            ></Backdrop>
        </div>
    );
};

export const basic = Basic.bind({});
