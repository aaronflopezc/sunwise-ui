import React from 'react';

import { Button, ButtonGroup } from '../.';

export default {
    title: 'Components/Inputs/ButtonGroup',
    component: ButtonGroup,
    argTypes: {
        color: {
            control: 'select',
            options: [
                'error',
                'info',
                'inherit',
                'primary',
                'secondary',
                'success',
                'warning',
            ],
            description: '',
            table: { defaultValue: { summary: 'primary' } },
        },
        disabled: {
            control: 'boolean',
            table: { defaultValue: { summary: true } },
        },
        disableElevation: {
            control: 'boolean',
            table: { defaultValue: { summary: true } },
        },
        size: {
            control: 'select',
            options: ['small', 'medium', 'large'],
            table: { defaultValue: { summary: 'medium' } },
        },
        variant: {
            control: 'select',
            options: ['text', 'contained', 'outlined'],
            table: { defaultValue: { summary: 'outlined' } },
        },
        orientation: {
            control: 'select',
            options: ['horizontal', 'vertical'],
            table: { defaultValue: { summary: 'horizontal' } },
        },
    },
};

const Template = ({ ...args }) => (
    <ButtonGroup {...args} orientation={args.orientation}>
        <Button>One</Button>
        <Button>Two</Button>
        <Button>Three</Button>
    </ButtonGroup>
);

export const Primary = Template.bind({});
