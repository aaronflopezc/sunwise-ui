import React from 'react';

import CheckIcon from '@mui/icons-material/Check';
import PropTypes from 'prop-types';

import { Alert, Button, AlertTitle } from '../.';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
    title: 'Components/Feedback/Alert',
    component: Alert,
    argTypes: {
        color: {
            control: 'select',
            options: [
                'error',
                'info',
                'inherit',
                'primary',
                'secondary',
                'success',
                'warning',
            ],
            description: '',
            table: { defaultValue: { summary: 'primary' } },
        },
        severity: {
            control: 'select',
            options: ['error', 'warning', 'info', 'success'],
        },
        variant: {
            control: 'select',
            options: ['standard', 'outlined', 'filled'],
        },
    },
};

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template = ({ title, ...args }) => (
    <Alert {...args}>
        {title && <AlertTitle>{title}</AlertTitle>}
        {args.children}
    </Alert>
);
Template.propTypes = {
    title: PropTypes.string,
};

export const Primary = Template.bind({});
Primary.args = {
    children: 'Text',
    severity: 'success',
};

export const withAlertTitle = Template.bind({});
withAlertTitle.args = {
    children: 'Text',
    severity: 'success',
    title: 'title',
};

export const withCloseButton = Template.bind({});
withCloseButton.args = {
    children: 'Text',
    severity: 'success',
    onClose: () => {},
};

export const withActionButton = Template.bind({});
withActionButton.args = {
    children: 'Text',
    severity: 'success',
    action: (
        <Button color="inherit" size="small">
            Action
        </Button>
    ),
};

export const Icons = Template.bind({});
Icons.args = {
    children: 'Text',
    severity: 'success',
    icon: <CheckIcon fontSize="inherit" />,
};
