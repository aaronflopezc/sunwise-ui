import React from 'react';

import { CssBaseline } from '@mui/material';
import {
    ThemeProvider,
    createTheme,
    StyledEngineProvider,
} from '@mui/material/styles';
import PropTypes from 'prop-types';

import breakpoints from './breakpoints';
import componentsOverride from './overrides';
import palette from './palette';
import shadows, { customShadows } from './shadows';
import shape from './shape';
import typography from './typography';

const ThemeConfig = ({ children }) => {
    const themeOptions = {
        breakpoints,
        customShadows,
        palette,
        shadows,
        shape,
        typography,
    };

    const theme = createTheme(themeOptions);
    theme.components = componentsOverride(theme);

    return (
        <StyledEngineProvider injectFirst>
            <ThemeProvider theme={theme}>
                <CssBaseline />
                {children}
            </ThemeProvider>
        </StyledEngineProvider>
    );
};

ThemeConfig.propTypes = {
    children: PropTypes.node,
};

export default ThemeConfig;
