import React from 'react';

import { Button } from '../.';

export default {
    title: 'Components/Inputs/Button',
    component: Button,
    argTypes: {
        children: { defaultValue: 'Button' },
        color: {
            control: 'select',
            options: [
                'error',
                'info',
                'inherit',
                'primary',
                'secondary',
                'success',
                'warning',
            ],
            description: '',
            table: { defaultValue: { summary: 'primary' } },
        },
        disabled: {
            control: 'boolean',
            table: { defaultValue: { summary: true } },
        },
        size: {
            control: 'select',
            options: ['small', 'medium', 'large'],
            table: { defaultValue: { summary: 'medium' } },
        },
        visible: {
            control: 'boolean',
            table: { defaultValue: { summary: true } },
        },
        variant: {
            control: 'select',
            options: ['text', 'contained', 'outlined'],
            table: { defaultValue: { summary: 'contained' } },
        },
    },
};

const Template = ({ ...args }) => <Button {...args} />;
export const Primary = Template.bind({});
