const Card = (theme) => ({
    MuiCard: {
        styleOverrides: {
            root: {
                borderRadius: theme.shape.borderRadiusMd,
                boxShadow: theme.customShadows.z16,
                marginBottom: theme.spacing(2),
            },
        },
    },
    MuiCardHeader: {
        defaultProps: {
            titleTypographyProps: { variant: 'h6' },
            subheaderTypographyProps: { variant: 'body2' },
        },
        styleOverrides: {},
    },
    MuiCardContent: {
        styleOverrides: {
            root: {},
        },
    },
});

export default Card;
