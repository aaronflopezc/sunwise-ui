import React from 'react';

import { Box, Link } from '../.';

export default {
    title: 'Components/Navigation/Link',
    component: Link,
    argTypes: {
        color: {
            control: 'select',
            options: ['error', 'inherit', 'primary', 'secondary'],
            description: '',
            table: { defaultValue: { summary: 'primary' } },
        },
        underline: {
            control: 'select',
            options: ['none', 'hover', 'always'],
            table: { defaultValue: { summary: 'primary' } },
        },
    },
};

const preventDefault = (event) => event.preventDefault();

const Basic = ({ ...args }) => (
    <Box
        sx={{
            display: 'flex',
            flexWrap: 'wrap',
            justifyContent: 'center',
            typography: 'body1',
            '& > :not(style) + :not(style)': {
                ml: 2,
            },
        }}
        onClick={preventDefault}
    >
        <Link href="#" {...args}>
            Link
        </Link>
        <Link href="#" {...args} color="inherit">
            {'color="inherit"'}
        </Link>
        <Link href="#" {...args} variant="body2">
            {'variant="body2"'}
        </Link>
    </Box>
);

export const basic = Basic.bind({});
