const Grid = () => ({
    MuiGrid: {
        defaultProps: {
            columns: 18,
            spacing: 2,
        },
        styleOverrides: {
            container: { marginTop: 1 },
            root: {
                '> .MuiCard-root': { marginBottom: 0 },
            },
        },
    },
});

export default Grid;
