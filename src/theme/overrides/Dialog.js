const Dialog = () => ({
    MuiDialogContent: {
        styleOverrides: { root: { overflowY: 'initial' } },
    },
});

export default Dialog;
