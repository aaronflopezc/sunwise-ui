# sunwise-ui

> Sunwise UI

[![NPM](https://img.shields.io/npm/v/sunwise-ui.svg)](https://www.npmjs.com/package/sunwise-ui) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save sunwise-ui
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'sunwise-ui'
import 'sunwise-ui/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

MIT © [Aaron Lopez](https://github.com/Aaron Lopez)
