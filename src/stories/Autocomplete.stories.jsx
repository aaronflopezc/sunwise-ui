import React from 'react';

import { Autocomplete, TextField } from '../.';

export default {
    title: 'Components/Inputs/Autocomplete',
    component: Autocomplete,
    argTypes: {
        color: {
            control: 'select',
            options: [
                'error',
                'info',
                'inherit',
                'primary',
                'secondary',
                'success',
                'warning',
            ],
            description: '',
            table: { defaultValue: { summary: 'primary' } },
        },
        variant: {
            control: 'select',
            options: ['standard', 'outlined', 'filled'],
            table: { defaultValue: { summary: 'outlined' } },
        },
    },
};

const Template = ({ ...args }) => (
    <Autocomplete
        options={args.options}
        renderInput={(params) => (
            <TextField
                {...params}
                color={args.color}
                label={args.label}
                placeholder={args.variant}
                variant={args.variant}
            />
        )}
    />
);

export const Primary = Template.bind({});
Primary.args = {
    label: 'Movie',
    placeholder: '',
    options: [
        { label: 'The Shawshank Redemption', year: 1994 },
        { label: 'The Godfather', year: 1972 },
        { label: 'The Godfather: Part II', year: 1974 },
        { label: 'The Dark Knight', year: 2008 },
        { label: '12 Angry Men', year: 1957 },
        { label: "Schindler's List", year: 1993 },
        { label: 'Pulp Fiction', year: 1994 },
    ],
};
