import React from 'react';

import Box from '@mui/material/Box';
import Divider from '@mui/material/Divider';
import PropTypes from 'prop-types';

const BottomActions = (props) => {
    const { children } = props;

    return (
        <>
            <Divider sx={{ mx: '-16px' }} />

            <Box
                display="flex"
                gap="16px"
                justifyContent="end"
                sx={{ '& .MuiButton-root': { mb: 0 } }}
            >
                {children}
            </Box>
        </>
    );
};

BottomActions.propTypes = {
    children: PropTypes.oneOfType([PropTypes.element, PropTypes.node]),
};

export default BottomActions;
