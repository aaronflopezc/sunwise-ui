import { merge } from 'lodash';

import Alert from './Alert';
import Autocomplete from './Autocomplete';
import Backdrop from './Backdrop';
import Button from './Button';
import Card from './Card';
import Dialog from './Dialog';
import Grid from './Grid';
import IconButton from './IconButton';
import Input from './Input';
import Paper from './Paper';
import Stack from './Stack';
import TextField from './TextField';
import Tooltip from './Tooltip';
import Typography from './Typography';

const ComponentsOverrides = (theme) =>
    merge(
        Alert(theme),
        Autocomplete(theme),
        Backdrop(theme),
        Button(theme),
        Card(theme),
        Dialog(theme),
        Grid(theme),
        IconButton(theme),
        Input(theme),
        Paper(theme),
        Stack(theme),
        TextField(theme),
        Tooltip(theme),
        Typography(theme)
    );

export default ComponentsOverrides;
