import React from 'react';

import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import PropTypes from 'prop-types';

const _Card = Card;
_Card.Body = CardContent;
_Card.Header = (props) => {
    const { children, sx, ...rest } = props;
    return (
        <CardContent
            {...rest}
            sx={{
                borderBottom: 1,
                borderColor: 'divider',
                padding: '16px !important',
                ...sx,
            }}
        >
            {children}
        </CardContent>
    );
};
_Card.Header.propTypes = {
    children: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    sx: PropTypes.object,
};
_Card.Header.displayName = 'Card.Header';
_Card.Media = CardMedia;
_Card.Actions = CardActions;

export default _Card;
